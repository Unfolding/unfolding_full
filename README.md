# DTA unfolding



## Getting started

Once the repo is cloned, fetch the dependencies using

```
git submodule update --init --recursive
```

### First time compilation

To set up and compile `RooUnfold` with cmake:

```
cd RooUnfold
mkdir build
cd build
cmake ..
make -j4
source setup.sh
cd ..
```
(also compare the documentation from [RooUnfold](https://gitlab.cern.ch/RooUnfold/RooUnfold/))

Then repeat similar steps for the `BootstrapGenerator`:

```
cd BootstrapGenerator
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=install
make
make install
source setup.sh
cd ..
```
(also compare the documentation from the [BootstrapGenerator](https://gitlab.cern.ch/atlas-physics/sm/StandardModelTools_BootstrapGenerator/BootstrapGenerator))



### Setup after a new login

You need to repeat the following steps (or just run the `init.sh` file provided):

```
setupATLAS
lsetup git
lsetup cmake
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh
export CMAKE_PREFIX_PATH=${PWD}/BootstrapGenerator/build/install
source RooUnfold/build/setup.sh
source BootstrapGenerator/build/setup.sh
```


### Unfolding preparation

The script expects the ROOT files coming from the grid to be merged with the [RescaleTool
tool](https://gitlab.cern.ch/atlas-physics/sm/wz/hmdy-tau/PostProcessing). They should be
either normalised to cross-section or number of events. If the former, the unfolding script
will multiply the luminosity automatically.

### Run Unfolding

The `DTA-unfold` script does all the magic.
Check out 

```
./DTA-unfold --help
```
for the available options.


### Running

`DTA-unfold` is the top-level execution file of the full workflow. \
`dta_model` contains the classes for read in files, data-preprocessing, preparation, scaling, stacking, etc. Act as the main data handling component. \
`dta_utils` contains the utility helper functions used across the framework. \
`dta_label` contains the label info for running information, e.g. categories of systematic uncertainties, BSM injection coupling, plotting labels and colour  schemes, name patterns for the files. \
`dta_plot`  contains the plotting functions for various stages of results. \
`dta_valid` contains all the validation methods which may not be part of the final production release, e.g. signal smoothing, bias calculation, bias significance, regression fitting, etc. 

