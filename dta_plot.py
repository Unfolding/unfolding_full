import ROOT as rt
rt.SetMemoryPolicy(rt.kMemoryStrict)
rt.gROOT.SetBatch(True)
rt.TH1F.SetDefaultSumw2(True)
rt.gErrorIgnoreLevel = rt.kWarning

import AtlasStyle
import dta_utils
AtlasStyle.setAtlasDefaults()
import dta_labels
import numpy as np
import ctypes, glob, random, string
from array import array
import collections
from scipy import stats
from scipy import ndimage
import math
import matplotlib.pyplot as plt
from dta_utils import LUMI
LUMI = None
def add_logo(x, y, channel, label = 'Internal', size = 0.07, ratio = True, syst = False):
    """add ATLAS plot logos, change the label argument for different purposes"""

    logo = rt.TLatex()
    logo.SetNDC()
    logo.SetTextColor(1)

    logo.SetTextFont(72)
    if syst:
       size = 0.04
    logo.SetTextSize( size )
  #  logo.DrawLatex(x, y, 'ATLAS')

    sf = size / 0.08
    dd = 0. if ratio else 0.05
    logo.SetTextFont(42)
    logo.SetTextSize( size * 0.85 )
    if syst:
       logo.DrawLatex(x + dd + sf * 0.20, y, label)
    else:
       logo.DrawLatex(x + dd + sf * 0.13, y, label)
    status = '#color[1]{#sqrt{s} = 13 TeV, %.1f fb^{-1}%s}' % (LUMI * 0.001, channel)
    logo.SetTextFont(42)
    logo.SetTextSize( size * 0.8 )
    logo.DrawLatex(x, y - sf * 0.08, status)
    logo.Clear()


#def DrawErrorBand()
def theory_err_plot(unfolded, logx=False, logy=True, theory_dict=None, breakdown=None,
                 xlabel='', ylabel='', ratio_max=None, ratio_min=None, theory_err=None):
    #TO DO: Simplify Ploting functions
    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    theory_colour = [rt.kBlue, rt.kPink, rt.kGreen, rt.kOrange, rt.kMagenta]
    unfolded.SetMarkerColor(rt.kBlack)
    unfolded.SetLineColor(rt.kBlack)
    unfolded.SetFillColor(0)

    h_split = 0.35
    PANEL1 = rt.TPad('main', '', 0.0, h_split, 1.0, 1.0)
    PANEL1.SetTickx()
    PANEL1.SetTicky()
    PANEL1.SetBottomMargin(0)
    PANEL1.SetRightMargin(0.05)
    PANEL1.Draw()
    PANEL1.cd()

    if logx:
        PANEL1.SetLogx()
    #remove the error of setting logy doesn't work because of zero content
    if logy:
        for i in range(1, unfolded.GetNbinsX() + 1):
            if unfolded.GetBinContent(i) <= 0:
                unfolded.SetBinContent(i, 1e-10)  # Set a small positive value
    
    if logy:
        PANEL1.SetLogy()

    unfolded.GetXaxis().SetTitle(xlabel)
    unfolded.GetYaxis().SetTitle(ylabel)
    #draw data
    unfolded.Draw('E1')
    
    #define legends
    leg = rt.TLegend(0.7, 0.7, 0.9, 0.9)
    leg.SetFillStyle(0)
    leg.SetTextColor(1)
    leg.SetTextFont(42)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    gr_unfolded = rt.TGraphErrors(unfolded.GetNbinsX())
    gr_unfolded_envelope = rt.TGraphAsymmErrors(unfolded.GetNbinsX())
    #Draw the initial plots
    for i in range(1, unfolded.GetNbinsX() + 1):
        x = unfolded.GetBinCenter(i)
        y = unfolded.GetBinContent(i)
        ex = unfolded.GetBinWidth(i) / 2
        ey = unfolded.GetBinError(i)
        ey_ratio_up, ey_ratio_dn = 0, 0
        gr_unfolded.SetPoint(i - 1, x, y)
        gr_unfolded.SetPointError(i - 1, ex, ey)
        gr_unfolded_envelope.SetPoint(i - 1, x, y)
        gr_unfolded_envelope.SetPointError(i - 1, ex, ex, ey, ey)

    gr_unfolded.SetMarkerColor(rt.kBlack)
    gr_unfolded.SetLineColor(rt.kBlack)
    gr_unfolded.SetFillColor(0)
    gr_unfolded.Draw("P SAME")

    gr_unfolded_envelope.SetFillColorAlpha(rt.kGray, 0.35)
    gr_unfolded_envelope.SetMarkerStyle(0)
    gr_unfolded_envelope.Draw("2 SAME")

    #if defined theory uncertainties, draw the error band (asymmetric) for each one
    if theory_dict:
        gr_theory_list, gr_theory_envelope_list, ratio_envelope_list = [], [], []
        for idx, (key, theory) in enumerate(theory_dict.items()):
            color = theory_colour[idx]
            theory.SetMarkerColor(color)
            theory.SetLineColor(color)
            theory.SetFillColor(0)
            theory.SetLineWidth(2)
            if key == 'SM (Sherpa 2.2)': theory.SetMarkerStyle(109)
            elif key == 'SM (PP8)': theory.SetMarkerStyle(113)
            leg.AddEntry(theory, key, 'LEP')
            theory.Draw('E1 SAME')

            gr_theory = rt.TGraphErrors(theory.GetNbinsX())
            gr_theory_envelope = rt.TGraphAsymmErrors(theory.GetNbinsX())
            ratio_err_cent = theory.Clone(dta_utils.randstr(5))
            dta_utils.softDivide(ratio_err_cent, unfolded)
            ratio_envelope = rt.TGraphAsymmErrors(ratio_err_cent.GetNbinsX())
            #set the error with the theory_err input, calculated from rivet
            for i in range(1, theory.GetNbinsX() + 1):
                x = theory.GetBinCenter(i)
                y = theory.GetBinContent(i)
                ratio_y = ratio_err_cent.GetBinContent(i)
                y_unfold = unfolded.GetBinContent(i)
                ex = theory.GetBinWidth(i) / 2
                if key in theory_err.keys():  
                  ey_up, ey_dn = theory_err[key][0][i-1], theory_err[key][1][i-1]
                  #print(key, i, ey_up, ey_dn)
                else:
                  ey_up, ey_dn = theory.GetBinError(i), theory.GetBinError(i)
               # print("The Key is ", key)

                ey_ratio_up, ey_ratio_dn = 0,0
                if ey_up and y_unfold:
                  ey_ratio_up = ey_up/y_unfold
                if ey_dn and y_unfold:
                  ey_ratio_dn = ey_dn/y_unfold
               # print(key, i, ey_ratio_up, ey_dn)
                
                gr_theory.SetPoint(i - 1, x, y)
                gr_theory_envelope.SetPoint(i - 1, x, y)
                gr_theory_envelope.SetPointError(i - 1, ex, ex, ey_up, ey_dn)
                ratio_envelope.SetPoint(i-1, x, ratio_y)
                ratio_envelope.SetPointError(i-1, ex, ex, ey_ratio_up, ey_ratio_dn)

            gr_theory.SetMarkerColor(color)
            gr_theory.SetMarkerSize(1)
            gr_theory.SetLineColor(color)
            gr_theory.SetFillColor(0)
            
           # gr_theory.Draw("P SAME")
            gr_theory_envelope.SetFillColorAlpha(color, 0.35)
            gr_theory_envelope.SetMarkerStyle(0)
            gr_theory_list.append(gr_theory)
            gr_theory_envelope_list.append(gr_theory_envelope)
            
            ratio_envelope.SetFillColorAlpha(color, 0.35)
            ratio_envelope.SetMarkerStyle(0)
            ratio_envelope_list.append(ratio_envelope)
         #   gr_theory_envelope.Draw("2 SAME")

        #Draw from a list to avoid ownership issues 
        for i in range(len(gr_theory_list)):
            gr_theory_list[i].Draw("P SAME")
            gr_theory_envelope_list[i].Draw("2 SAME")

    leg.AddEntry(unfolded, 'Unfolded data', 'LEP')
    leg.Draw()

    CANVAS.cd()
    PANEL2 = rt.TPad('ratio1', '', 0.0, 0.0, 1.0, h_split)
    PANEL2.SetTickx()
    PANEL2.SetTicky()
    PANEL2.SetTopMargin(0)
    PANEL2.SetBottomMargin(0.25)
    PANEL2.SetRightMargin(0.05)
    PANEL2.Draw()
    PANEL2.cd()

    ratioRef = unfolded.Clone(dta_utils.randstr(5))
    dta_utils.softDivide(ratioRef, unfolded)
    ratioRef.GetYaxis().SetTitle('Prediction / Data')
    ratioRef_envelope = rt.TGraphAsymmErrors(unfolded.GetNbinsX())
    for i in range(1, unfolded.GetNbinsX() + 1):   
         x = unfolded.GetBinCenter(i)
         y = ratioRef.GetBinContent(i)
         ex = theory.GetBinWidth(i) / 2
         ey = ratioRef.GetBinError(i)
         ratioRef_envelope.SetPoint(i-1, x, y)
         ratioRef_envelope.SetPointError(i-1, ex, ex, ey, ey)

    ratioRef.SetMaximum(ratio_max if ratio_max else 1.29)
    ratioRef.SetMinimum(ratio_min if ratio_min else 0.7)
    ratioRef_envelope.SetFillColorAlpha(rt.kGray, 0.35)
    ratioRef.GetYaxis().SetTitleSize(0.12)
    ratioRef.GetYaxis().SetTitleOffset(0.6)
    ratioRef.GetYaxis().SetLabelSize(0.08)

    ratioRef.GetXaxis().SetTitle(xlabel)
    ratioRef.GetXaxis().SetTitleSize(0.12)
    ratioRef.GetXaxis().SetTitleOffset(0.8)
    ratioRef.GetXaxis().SetLabelSize(0.08)
    ratioRef.Draw('P')
    ratioRef_envelope.Draw('2 SAME')
    ratio_list = []
    #draw for the ratio panel 
    for idx, (key, theory) in enumerate(theory_dict.items()):
        ratio = theory.Clone(dta_utils.randstr(5))
        ratio.SetMarkerColor(theory_colour[idx])
        ratio.SetLineColor(theory_colour[idx])
        if key == 'SM (Sherpa DY+ttbar)': ratio.SetMarkerStyle(109) #up triangle
        elif key == 'SM (PP8 DY+ttbar)': ratio.SetMarkerStyle(113)  #dn triangle
        dta_utils.softDivide(ratio, unfolded)
        ratio_list.append(ratio)

    for r in range(len(ratio_list)):
        ratio_list[r].Draw('P SAME')
        ratio_envelope_list[r].Draw('2 SAME')
    PANEL2.Update()

    #draw y = 1.0 line        
    line = rt.TLine(PANEL2.GetUxmin(), 1.0, PANEL2.GetUxmax(), 1.0)
    line.SetLineColor(rt.kBlue)
    line.Draw("same")

    CANVAS.SaveAs('output/test_with_theory_error_bands.pdf')



#1DratioPlot location
def ratioPlot1D(h_denom, h_numer, fileName, ch, obs, logx = 0, logy = 0,
                                  xlabel = '', ylabel = '', ratio_min = None, ratio_max = None, data = False,
                                  bias = False, visible = False, reco = False, unfolded = False, modelling = False, resp = False,
                                  logo = None, leg_label_denom = None, leg_label_numer = None, form = None, outdir = 'output', poly_fit=0., poly_fit_mll=0.):
    """specialised plotting function with upper panel as the two distributions for comparison and the lower panel as the ratio
       the ratio is h_numer / h_denom"""
    if not form: 
       print(f"WARNING: please specify a output format for plot {fileName}, default pdf is used for now")
       form = ['pdf']

    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    is2D = '_vs_' in fileName

    h_denom.SetMarkerColor(rt.kBlack)
    h_denom.SetLineColor(rt.kBlack)
    h_denom.SetFillColor(0)

    h_numer.SetMarkerColor(rt.kAzure+2)
    h_numer.SetLineColor(rt.kAzure+2)
    h_numer.SetFillColor(0)
    
    hsplit = 0.35
    PANEL1 = rt.TPad('main', '', 0.0, hsplit, 1.0, 1.0,)
    PANEL1.SetTickx()
    PANEL1.SetTicky()
    PANEL1.SetBottomMargin(0)
    PANEL1.SetRightMargin(0.05)
    PANEL1.SetLogx(logx)
    PANEL1.SetLogy(logy)
    PANEL1.Draw()
    PANEL1.cd()
    h_numer.GetXaxis().SetTitle(xlabel)
    h_numer.GetYaxis().SetTitle(ylabel)
    
    h_numer.Draw()
    h_denom.Draw('same')
    PANEL1.Update()
    leg = rt.TLegend(0.2 if is2D else 0.77, 0.77,  # (xlo, ylo)
                     0.4 if is2D else 0.9, 0.9)  # (xhi, yhi)
    leg.SetFillStyle(0)
    leg.SetTextColor(1)
    leg.SetTextFont(42)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    if leg_label_denom:
        leg.AddEntry(h_denom, leg_label_denom, 'LEP')
        leg.AddEntry(h_numer, leg_label_numer, 'LEP')
    else:
       if bias:
         if visible:
           leg.AddEntry(h_denom, 'Nominal visible_truth', 'LEP')
           leg.AddEntry(h_numer, 'Biased visible_truth', 'LEP')
         if reco:
           leg.AddEntry(h_denom, 'Nominal reco-level', 'LEP')
           leg.AddEntry(h_numer, 'Biased reco-level', 'LEP')
         elif not visible and not reco and not unfolded:
           leg.AddEntry(h_denom, 'Nominal truth_predecay', 'LEP')
           leg.AddEntry(h_numer, 'Biased truth_predecay', 'LEP')
         elif unfolded:
           leg.AddEntry(h_denom, 'Nominal unfolded', 'LEP')
           leg.AddEntry(h_numer, 'bias unfolded', 'LEP')
       elif modelling:
           leg.AddEntry(h_denom, 'ZTT PP8 unfolded', 'LEP')
           leg.AddEntry(h_numer, 'TTBAR PP8 unfolded', 'LEP')
       elif resp:
           leg.AddEntry(h_denom, 'pre-smooth', 'LEP')
           leg.AddEntry(h_numer, 'after-smooth', 'LEP')

       else:
          leg.AddEntry(h_denom, 'Pseudodata', 'LEP')
          leg.AddEntry(h_numer, 'Standard Model', 'LEP') #change to biased standard model when bias
    if poly_fit or poly_fit_mll:
      ratio_fit_check, ratio_fit_check2, ratio_fit_check3 = h_numer.Clone(dta_utils.randstr(5)), h_numer.Clone(dta_utils.randstr(5)), h_numer.Clone(dta_utils.randstr(5))
      ratio_fit_check.SetMarkerColorAlpha(rt.kGreen, 0.5)
      ratio_fit_check2.SetMarkerColorAlpha(rt.kGreen, 0.5)
      ratio_fit_check.SetMarkerStyle(rt.kMultiply)
      ratio_fit_check2.SetMarkerStyle(rt.kMultiply)
      ratio_fit_check.SetLineColorAlpha(rt.kGreen, 0.5)
      ratio_fit_check2.SetLineColorAlpha(rt.kGreen, 0.5)
      
      ratio_fit_check3.SetMarkerColorAlpha(rt.kGreen, 0.5)
      ratio_fit_check3.SetMarkerStyle(rt.kMultiply)
      ratio_fit_check3.SetLineColorAlpha(rt.kGreen, 0.5)

      leg.AddEntry(ratio_fit_check, 'Reweighted reco')
      leg.AddEntry(ratio_fit_check2, 'Reweighted reco2')
      leg.AddEntry(ratio_fit_check2, 'Reweighted reco3')
    leg.Draw('same')

  #  if fitting_params1:
  #     x_axis = [ h_numer.GetBinCenter(i) for i in range(h_numer.GetNbinsX() + 2 ]
  #     x_axis1 = x_axis[1:7]
  #     x_axis2 = x_axis[10:] 
  #     poly_func = ROOT.TF1("poly_func", "fitting_params1[0]*x**3 + fitting_params1[1]*x**2 + fitting_params1[2]*x + fitting_params1[3]", h_numer.GetXaxis().GetXmin(), h_data.GetXaxis().GetXmax())



    if logo:
      dta_utils.add_logo(*logo, dta_utils.get_channel(fileName))
    h_numer.Draw('sameaxis')

    CANVAS.cd()
    PANEL2 = rt.TPad('ratio1', '', 0.0, 0.0, 1.0, hsplit)
    PANEL2.SetTickx()
    PANEL2.SetTicky()
    PANEL2.SetTopMargin(0)
    PANEL2.SetBottomMargin(0.42 if is2D else 0.25)
    PANEL2.SetRightMargin(0.05)
    PANEL2.SetLogx(logx)
    PANEL2.SetLogy(0)
    PANEL2.Draw()
    PANEL2.cd()

    if poly_fit or poly_fit_mll:
      ratioRef = h_numer.Clone()
      dta_utils.softDivide(ratioRef, h_numer)
      ratio = h_denom.Clone(dta_utils.randstr(5))
    #  ratio2 = ratio.Clone(dta_utils.randstr(5))
      dta_utils.softDivide(ratio, h_numer)#This is data/reco
      #dta_utils.softDivide(ratio2, h_numer) 
      param_list, param_list2, param_list3 = [], [], []
      if poly_fit:
         poly_param = ratio.Fit('pol' + poly_fit, 'SR', 'same', 160, 13000 )
         for nparam in range(int(poly_fit)+1):
           param_list.append(poly_param.Get().Parameter(nparam))
         ratio_fit_check = dta_utils.ptllRW(h_denom, h_numer, ratio_fit_check, param_list)
      else:
         poly_param = ratio.Fit('pol' + '2', 'SR', 'same', 100, 190 )
         poly_param2 = ratio.Fit('pol' + '1', 'SR', 'same', 190, 260 )
         poly_param3 = ratio.Fit('pol' + poly_fit_mll, 'SR', 'same', 420, 13000 )
         for nparam in range(int(poly_fit_mll)+1):
           param_list.append(poly_param.Get().Parameter(nparam))
           param_list2.append(poly_param2.Get().Parameter(nparam)) 
           param_list3.append(poly_param3.Get().Parameter(nparam))
         ratio_fit_check = dta_utils.RW(h_denom, h_numer, ratio_fit_check, param_list, start=1, end=4)
         ratio_fit_check2 = dta_utils.RW(h_denom, h_numer, ratio_fit_check2, param_list2, start=4, end=6)
         ratio_fit_check3 = dta_utils.RW(h_denom, h_numer, ratio_fit_check3, param_list3, start=10)
 
      ratio_max = 1.8
      ratio_min = 0.6
      ratioRef.GetYaxis().SetTitle('Data / Prediction') 
      
    else:
      ratio = h_numer.Clone()
      dta_utils.softDivide(ratio, h_denom)
      ratioRef = h_denom.Clone()
      dta_utils.softDivide(ratioRef, h_denom)
      ratioRef.GetYaxis().SetTitle('Prediction / Data')
    
    ratioRef.GetYaxis().SetTitleSize(0.12)
    ratioRef.GetYaxis().SetTitleOffset(0.6)
    ratioRef.GetYaxis().SetLabelSize(0.08)

    ratioRef.GetXaxis().SetTitle(xlabel)
    ratioRef.GetXaxis().SetTitleSize(0.12)
    ratioRef.GetXaxis().SetTitleOffset(1.7 if is2D else 0.8)
    ratioRef.GetXaxis().SetLabelSize(0.08)

    if is2D:
      ratioRef.LabelsOption('v')
   # ratio.SetMinimum(0.5)
   # ratio.SetMaximum(1.8)
    ratioRef.SetMaximum(ratio_max) if ratio_max else ratio.SetMaximum(1.29)
    ratioRef.SetMinimum(ratio_min) if ratio_min else ratio.SetMinimum(0.7)
    ratioRef.Draw()
    ratio.Draw('same')
    if poly_fit or poly_fit_mll:
      ratio_fit_check.Draw('same')
      #PANEL2.Update()
      ratio_fit_check2.Draw('same')
      ratio_fit_check3.Draw('same')
    PANEL2.Update()
    line = rt.TLine(PANEL2.GetUxmin(), 1.0, PANEL2.GetUxmax(), 1.0)
    line.SetLineColor(rt.kBlue)
    line.Draw("same")
    
    for f in form:
       CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/' + fileName + '.' + f)
    PANEL1.Close()
    PANEL2.Close()
    CANVAS.Close()
    ratio.Delete()





def eff1D(graph, hist, fileName, ch, obs, logx = 0, logy = 0, yupper = None, ylower = None,
                       xlabel = '', ylabel = '', logo = None, form = None, outdir = 'output'):
    if not form: 
       print(f"WARNING: please specify a output format for plot {fileName}, default pdf is used for now")
       form = ['pdf']

    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    is2D = '_vs_' in fileName

    CANVAS.SetLogx(logx)
    CANVAS.SetLogy(logy)
    CANVAS.SetTickx()
    CANVAS.SetTicky()
    if is2D:
      CANVAS.SetBottomMargin(0.2)

    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)

    if ylower != None:
      hist.SetMinimum(ylower)
    if yupper != None:
      hist.SetMaximum(yupper)

    hist.GetYaxis().SetTitleSize(0.05)
    hist.GetYaxis().SetTitleOffset(0)
    hist.GetYaxis().SetLabelSize(0.04)

    hist.GetXaxis().SetTitleSize(0.05)
    hist.GetXaxis().SetTitleOffset(1.8 if is2D else 0.8)
    hist.GetXaxis().SetLabelSize(0.03)
    hist.GetXaxis().SetLabelOffset(0.01)

    if is2D:
      hist.LabelsOption('v')
    hist.Draw('axis')
    graph.Draw('zpsame')

    if logo:
      dta_utils.add_logo(*logo, dta_utils.get_channel(fileName), size = 0.05, ratio = False)

    hist.Draw('sameaxis')
    for f in form:
       CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/%s.' % fileName + f)
    CANVAS.Close()

def find_curve_ratios(full, reduced, ch, obs, leg_labels = None, xlabel = '', ylabel = '', yupper = None, ylower = None):
    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    l = [ ]
    for h in range(len(full)):
      hist = reduced[h].Clone(dta_utils.randstr(5))
      for i in range(hist.GetNbinsX() + 2):
         if hist.GetBinContent(i) <= 0 or full[h].GetBinContent(i) <= 0:
             hist.SetBinContent(i, 0)
         else:
            bin_val = hist.GetBinContent(i) / full[h].GetBinContent(i)
            hist.SetBinContent(i, 100*bin_val)
      l.append(hist)

    hist = l[0]
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)

    if ylower != None:
      hist.SetMinimum(ylower)
    if yupper != None:
      hist.SetMaximum(yupper)

    hist.GetYaxis().SetTitleSize(0.05)
    hist.GetYaxis().SetTitleOffset(0)
    hist.GetYaxis().SetLabelSize(0.04)

    hist.GetXaxis().SetTitleSize(0.05)
    hist.GetXaxis().SetTitleOffset(0.8)
    hist.GetXaxis().SetLabelSize(0.03)
    hist.GetXaxis().SetLabelOffset(0.01)

    hist.Draw()

    for h in l[1:]:
       h.Draw("same hist")

    if leg_labels != None:
      assert( len(full) == len(leg_labels) )
      leg = rt.TLegend(0.7, 0.7,  # (xlo, ylo)
                       0.9, 0.9)  # (xhi, yhi)
      leg.SetFillStyle(0)
      leg.SetTextColor(1)
      leg.SetTextFont(42)
      leg.SetTextSize(0.04)
      leg.SetBorderSize(0)
      leg.SetFillColor(0)
      for i in range(len(l)):
        leg.AddEntry(l[i], leg_labels[i], 'L')
      leg.Draw('same')

   # hist.Draw('sameaxis')
    for f in form:
       CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/syst_ratio.' + f)
    CANVAS.Close()
    for curve in l:
        curve.Delete()

def systBreakdown(curves, fileName, ch, obs, logx = 0, logy = 0, yupper = None, ylower = None,
                           xlabel = '', ylabel = '', leg_labels = None, logo = None,
                           err = None, skip_model = False, bias_test = False, panel = False, form = None, outdir = 'output'):

    """not only just for systematic break down, this function is now plotting for any purpose of ATLAS plot
       with multiple curves for trend comparison, etc."""

    if not form: 
       print(f"WARNING: please specify a output format for plot {fileName}, default pdf is used for now")
       form = ['pdf']

    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)

    is2D = '_vs_' in fileName
    if is2D:
      CANVAS.SetBottomMargin(0.2)

    CANVAS.SetLogx(logx)
    CANVAS.SetLogy(logy)
    CANVAS.SetTickx()
    CANVAS.SetTicky()

    if panel:

       hsplit = 0.35
       PANEL1 = rt.TPad('main', '', 0.0, hsplit, 1.0, 1.0,)
       PANEL1.SetTickx()
       PANEL1.SetTicky()
       PANEL1.SetBottomMargin(0)
       PANEL1.SetRightMargin(0.05)
       PANEL1.SetLogx(logx)
       PANEL1.SetLogy(logy)
       PANEL1.Draw()
       PANEL1.cd()

    hist = curves[0]
    hist.GetXaxis().SetTitle(xlabel)
    hist.GetYaxis().SetTitle(ylabel)
    hist.GetXaxis().ChangeLabel(-1,-1,-1,-1,-1,-1,'13 TeV')

    if ylower != None:
      hist.SetMinimum(ylower)
    else:
      hist.SetMinimum(hist.GetMinimum())
    if yupper != None:
      hist.SetMaximum(yupper)
    else:
      hist.SetMaximum(hist.GetMaximum())

    hist.GetYaxis().SetTitleSize(0.05)
    hist.GetYaxis().SetTitleOffset(0)
    hist.GetYaxis().SetLabelSize(0.04)

    hist.GetXaxis().SetTitleSize(0.05)
    hist.GetXaxis().SetTitleOffset(1.8 if is2D else 0.8)
    hist.GetXaxis().SetLabelSize(0.03)
    hist.GetXaxis().SetLabelOffset(0.01)

    if is2D:
      hist.LabelsOption('v')
    hist.Draw('Hist')

    if bias_test:
      colour = [rt.kMagenta, rt.kBlue, rt.kGreen, rt.kRed, rt.kOrange+4, rt.kAzure+4, rt.kAzure+8, rt.kViolet+6, rt.kViolet+2, rt.kPink+10, rt.kPink-7, rt.kRed-6, rt.kYellow+2]
      for i in range(1, len(curves)):
         curves[i].SetLineColor(colour[i])
         curves[i].SetMarkerColor(colour[i])

    for h in curves[1:]:
      h.Draw('same Hist')

    if leg_labels != None:
      assert( len(curves) == len(leg_labels) )
      leg = rt.TLegend(0.7, 0.6,  # (xlo, ylo)
                       0.9, 0.9)  # (xhi, yhi)
      leg.SetFillStyle(0)
      leg.SetTextColor(1)
      leg.SetTextFont(42)
      leg.SetTextSize(0.04)
      leg.SetBorderSize(0)
      leg.SetFillColor(0)
      for i in range(len(curves)):
        leg.AddEntry(curves[i], leg_labels[i], 'L')
      leg.Draw('same')

    if logo:
      dta_utils.add_logo(*logo, dta_utils.get_channel(fileName), size = 0.05, ratio = False)

    hist.Draw('sameaxis')
    line = rt.TLine(CANVAS.GetUxmin(), 0.5, CANVAS.GetUxmax(), 0.5)
    line.SetLineColor(rt.kBlue)
    line.Draw("same")

    if panel:
       CANVAS.cd()
       PANEL2 = rt.TPad('ratio1', '', 0.0, 0.0, 1.0, hsplit)
       PANEL2.SetTickx()
       PANEL2.SetTicky()
       PANEL2.SetTopMargin(0)
       PANEL2.SetBottomMargin(0.42 if is2D else 0.25)
       PANEL2.SetRightMargin(0.05)
       PANEL2.SetLogx(logx)
       PANEL2.SetLogy(0)
       PANEL2.Draw()
       PANEL2.cd()

       first_iter = curves[0].Clone(dta_utils.randstr(5))
       first_iter.GetXaxis().SetTitleSize(0.08)
       first_iter.GetXaxis().SetLabelSize(0.07)
       first_iter.GetYaxis().SetTitleSize(0.08)
      # first_iter.Divide(hist)
       first_iter.GetYaxis().SetTitle("difference to 1 iter")
      # first_iter.SetMaximum(0.4)
      # first_iter.SetMinimum(0)
      # first_iter.Divide(hist)
       first_iter.Add(hist, -1)
       first_iter.SetMaximum(0.4)
       first_iter.GetXaxis().SetRangeUser(100, 1000)  ###
       first_iter.Draw('Hist')
       ratios = [ ]
       for i in range(1, len(curves)):
           iters = curves[i].Clone(dta_utils.randstr(5))
           iters.Add(hist, -1)
           #iters.Divide(hist)
           ratios.append(iters)
       for c in ratios:
           c.Draw("same Hist")
    for f in form:
       CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/%s.' % fileName + f)
    if panel:
       PANEL1.Close()
       PANEL2.Close()
    CANVAS.Close()



def plot2D(histo2D, fileName, ch, obs, logx = 0, logy = 0, logz = 0,
                              xlabel = '', ylabel = '', form = None, outdir = 'output'):
    if not form: 
       print(f"WARNING: please specify a output format for plot {fileName}, default pdf is used for now")
       form = ['pdf']
    #hist2D = dta_utils.gauss_kernel_2D(histo2D, 5, is2D = True, cleaned = True)#single 5 for now
    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    is2D = '_vs_' in fileName

    # CANVAS.SetLogx(logx)
    # CANVAS.SetLogy(logy)
    # CANVAS.SetLogz(logz)
    CANVAS.SetTickx()
    CANVAS.SetTicky()
    CANVAS.SetRightMargin(0.15)
    if is2D:
      CANVAS.SetLeftMargin(0.18)
      CANVAS.SetBottomMargin(0.18)
    

    histo2D.GetXaxis().SetTitle(xlabel)
    histo2D.GetYaxis().SetTitle(ylabel)
    histo2D.SetMinimum(0)
    histo2D.SetMaximum(1)

    histo2D.GetXaxis().SetTitleSize(0.05)
    histo2D.GetXaxis().SetTitleOffset(1.8 if is2D else 0.8)
    histo2D.GetXaxis().SetLabelSize(0.03)
    histo2D.GetXaxis().SetLabelOffset(0.01)

    x_pos = histo2D.GetXaxis().GetBinLowEdge(histo2D.GetXaxis().FindBin(635))
    line = rt.TLine(x_pos, histo2D.GetMinimum(), x_pos, histo2D.GetMaximum() * 0.98)
    
    dash_start = histo2D.GetXaxis().GetBinLowEdge(histo2D.GetXaxis().FindBin(635))
    dash_end = histo2D.GetXaxis().GetBinLowEdge(histo2D.GetXaxis().FindBin(13000))
    dashed_line = rt.TLine(dash_start, histo2D.GetMaximum() * 0.98, dash_end, histo2D.GetMaximum() * 0.98)
    dashed_line.SetLineStyle(2)  # Set dashed line style


    histo2D.GetYaxis().SetTitleSize(0.05)
    histo2D.GetYaxis().SetTitleOffset(1.8 if is2D else 1.0)
    histo2D.GetYaxis().SetLabelSize(0.03)
    histo2D.GetYaxis().SetLabelOffset(0.005)
    CANVAS.Update()

    if is2D:
      histo2D.LabelsOption('v')
   # histo2D.Draw("hcolz TEXT")
    histo2D.Draw("hcolz")
    line.Draw()
    dashed_line.Draw()
    for f in form:
       CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/' + fileName + '.' + f)
    CANVAS.Close()


def plt_plot(ratio, fit):
    val = np.array(ratio)[1:-1]
    bin_centre = np.zeros(len(val))
    for i in range(ratio.GetNbinsX() + 2):
        if not i > 26 and i != 0:
           bin_centre[i-1] = ratio.GetBinCenter(i)
    plt.plot(bin_centre, val)
    plt.plot(bin_centre, fit, color='r')
    return plt.savefig('ratio.png')


def plot1D(histo1D, filename, ch, obs, logx = 0, logy = 0, yupper = None, ylower = None,
                              xlabel = '', ylabel = '', form = None, outdir = 'output'):
    if not form: 
       print(f"WARNING: please specify a output format for plot {fileName}, default pdf is used for now")
       form = ['pdf']

    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    CANVAS.SetTickx()
    CANVAS.SetTicky()

    histo1D.GetXaxis().SetTitle(xlabel)
    histo1D.GetYaxis().SetTitle(ylabel)

    histo1D.SetMaximum(histo1D.GetMaximum())
    histo1D.SetMinimum(histo1D.GetMinimum())
    if ylower != None:
      histo1D.SetMinimum(ylower)
    if yupper != None:
      histo1D.SetMaximum(yupper)
    histo1D.Draw()
    for f in form:
       CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/' + filename + '.' + f)
    CANVAS.Close()


def ratioPlotSyst(syst_dict, fileName, ch, obs, xlabel, ylabel, logo = None, form = None, outdir = 'output'):
    """plot the systematic breakdown plots with customised plot settings"""
    if not form: 
       print(f"WARNING: please specify a output format for plot {fileName}, default pdf is used for now")
       form = ['pdf']
    is2D = "_vs_" in fileName
    for syst_set in syst_dict:
       CANVAS = rt.TCanvas('c', 'canvas', 800, 600)
       if "TRUEHADTAU_EFF" in syst_set:
          leg = rt.TLegend(0.65,0.2,0.95,0.5)  # (xlo, ylo))  # (xhi, yhi)
          leg2 = rt.TLegend(0.2, 0.7, 0.55, 0.95)
          leg2.SetFillStyle(0);
          leg2.SetTextColor(1)
          leg2.SetTextFont(42)
          leg2.SetTextSize(0.02)
          leg2.SetBorderSize(0)
       elif "TRUEHADTAU_SME" in syst_set:
          leg = rt.TLegend(0.6,0.2,0.95,0.5)
       else:
          leg = rt.TLegend(0.65,0.2,0.95,0.5)
     # leg.SetFillStyle(0)
       leg.SetTextColor(1)
       leg.SetTextFont(42)
       leg.SetTextSize(0.02)
       leg.SetBorderSize(0)
       leg.SetFillStyle(0);
       j = 0
       double = False
       for i in range(len(syst_dict[syst_set])):
          var, ratio = syst_dict[syst_set][i][0], syst_dict[syst_set][i][1]
          if "TRIGGER_STATMC2016" in var: continue
          if "TRIGGER_MC2016" in var: continue
     #     if "TRIGGER_STATMC161718" in var: continue
          if "TRIGGER_STATDATA2016" in var: continue
     #     if "TRIGGER_STATDATA161718" in var: continue
          if j == 9: j = 11
          ratio.SetLineColor(rt.kBlack + j)
          ratio.SetMinimum(0.85)
          ratio.SetMaximum(ratio.GetMaximum() + 0.05)
          if "TAUS_TRUEHADTAU_SME" in var:
              ratio.SetMinimum(0.5)
          #    ratio.SetMaximum(1.20)
          elif "MUON_ID" in var:
              ratio.SetMinimum(0.80)
          #    ratio.SetMaximum(1.20)
          elif "TRUEHADTAU_EFF" in var:
              ratio.SetMinimum(0.94)
          #elif "MUON_EFF" in var:
          #    ratio.SetMinimum(0.98)
          #    ratio.SetMaximum(1.02)
          ratio.GetXaxis().SetTitle(xlabel)
          ratio.GetYaxis().SetTitle(ylabel)
          ratio.GetXaxis().SetTitleSize(0.04)
          ratio.GetYaxis().SetTitleSize(0.04)
          ratio.GetYaxis().SetTitleOffset(1.6)
          ratio.GetXaxis().SetTitleOffset(1.6)
          if "up" in var:
            # if "TRUEHADTAU" in var:
            #    leg.AddEntry(ratio, "".join(var.split('_')[-4:]), 'syst' + var)
            # else:
             if "TRUEHADTAU_EFF" in var:
               if not i > len(syst_dict[syst_set]) // 2:
                 leg.AddEntry(ratio, "     " + "_".join(var.replace("__", "_").split("_")[-3:-1]), 'L' )#'syst' + var, 'L')
               else:
                 leg2.AddEntry(ratio, "      " + "_".join(var.replace("__", "_").split("_")[-3:-1]), 'L') #'syst' + var, 'L')
                 double = True
             else:
                 leg.AddEntry(ratio, "_".join(var.replace("__", "_").split('_')[0:-1]), 'L')#'syst' + var, 'L')
             j += 1
          ratio.Draw("same hist")
       if logo:
           dta_utils.add_logo(*logo, dta_utils.get_channel(fileName), syst = True)
       leg.Draw('same')
       if double:
           leg2.Draw('same')
       for f in form:
          CANVAS.SaveAs(outdir+'/' + ch + '_' + obs + '/%s.' % syst_set + f)
       CANVAS.Close()


