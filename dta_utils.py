
import ROOT as rt
rt.SetMemoryPolicy(rt.kMemoryStrict)
rt.gROOT.SetBatch(True)
rt.TH1F.SetDefaultSumw2(True)
rt.gErrorIgnoreLevel = rt.kWarning

import AtlasStyle
AtlasStyle.setAtlasDefaults()

import numpy as np
import ctypes, glob, random, string, math
import collections
from array import array
from scipy import stats
import scipy.signal as sp
import dta_labels
import heapq
LUMI = None


def raiseError(txt):
    sys.exit('\033[93m' + txt + '\033[93m')


def printInfo(*args):
    if len(args) == 1:
        print('\033[92m' + args[0] + '\033[0m')
    else:
        print('\033[93m', *args, '\033[0m')

def printDebug(*args):
    if len(args) == 1:
        print('\033[93m' + args[0] + '\033[0m')
    else:
        print('\033[93m', *args, '\033[0m')

def printWarning(*args):
    if len(args) == 1:
        print('\033[96m' + args[0] + '\033[0m')
    else:
        print('\033[96m', *args, '\033[0m')


def add_logo(x, y, channel, label = 'ATLAS Internal', size = 0.07, ratio = True, syst = False):
    """add ATLAS plot logos, change the label argument for different purposes"""

    logo = rt.TLatex()
    logo.SetNDC()
    logo.SetTextColor(1)

    logo.SetTextFont(72)
    if syst:
       size = 0.04
    logo.SetTextSize( size )
    logo.DrawLatex(x, y, 'ATLAS')

    sf = size / 0.08
    dd = 0. if ratio else 0.05
    logo.SetTextFont(42)
    logo.SetTextSize( size * 0.85 )
    if syst:
       logo.DrawLatex(x + dd + sf * 0.20, y, label)
    else:
       logo.DrawLatex(x + dd + sf * 0.13, y, label)

   # unit = "fb" if LUMI > 140 else "pb"
    LUMI_VAL = LUMI*0.001 if LUMI > 140 else LUMI
    status = '#color[1]{#sqrt{s} = 13 TeV, %.1f fb^{-1}%s}' % (LUMI_VAL, channel)
    logo.SetTextFont(42)
    logo.SetTextSize( size * 0.8 )
    logo.DrawLatex(x, y - sf * 0.08, status)
    logo.Clear()


def progress_bar(progress, total):
   percent = 100 * (progress / total)
   bar = u"\u25A0" * int(percent) + '-' * int(100 - percent)
   print(f"\033[93m\r|{bar}| {percent:.2f}%\033[0m", end = '\r')
   if progress == total:
      print('\033[93mComplete!\033[0m', end = '\n')


def randstr(k):
  '''Generates a random all-lower-case-ascii
     string of length k and returns it.'''
  return ''.join(random.choices(string.ascii_lowercase, k=k))



def safeDiv(numerator, denominator, fallback = 0.0):
  with np.errstate(divide='ignore', invalid='ignore'): # ignore warnings about division by zero
    ratio = numerator / denominator
  ratio[ ~ np.isfinite(ratio) ] = fallback # account for division by zero
  return ratio


def get_error(err_list):
   N = len(err_list)
   S = np.zeros(len(err_list[0]))
   for i in err_list:
       S += i
   sq_sum = S**2
   return np.sqrt(sq_sum/N)


def fit_ratio_polynomial(data, reco):
   N = reco.GetNbinsX() + 2
   x_bin = np.zeros(N-2)
   y_val = np.zeros(N-2)
   ratio = data.Clone(randstr(5))
   for i in range(N):
      data_val  = data.GetBinContent(i)
      reco_val  = reco.GetBinContent(i)
   #   print(i, N)
  #    print(data_val, reco_val)
      if data_val and reco_val:
   #      print(i)
         ratio_val = data_val/reco_val
      else:
         ratio_val = 0.0
  #    ratio.SetBinContent(i, ratio_val)
      if not i > 26 and i != 0:
         y_val[i-1] = ratio_val
         x_bin[i-1] = data.GetBinCenter(i)
   polyfit = np.polyfit(x_bin, y_val)
   polyfit = linear_reg(x_bin, y_val)
##   polyfit_x, polyfit_y, fit_score = multi_reg(x_bin, y_val)
   #res = stats.goodness_of_fit(ployfit_y, y_val, statistic='ks')
   #print('ks test results = ' + 'stats: ' + str(res.statistic), 'pval =' + str(res.pvalue))
   return polyfit_y#, fit_score

def apply_poly_scale(h_truth, polyfit_ratio, llpT):
   N = h_truth.GetNbinsX() + 2
   bin_val = np.array(h_truth)[1:-1]
   llpT_bins = np.zeros(N-2)
   for i in range(1, N-2):
      llpT_bins[i] = llpT.GetBinCenter(i)
#   truth_val_map = map(bin_val, truth_bins)
   llpT_val_map  = zip(llpT_bins, polyfit_ratio)
   for b, val in llpT_val_map:
      target_bin = h_truth.FindBin(b)
      origin_val = h_truth.GetBinContent(target_bin)
      if not origin_val:
         h_truth.SetBinContent(target_bin, 0)
      else:
         h_truth.SetBinContent(target_bin, origin_val*val)

#   scaled_val = bin_val*polyfit
#   for i in range(1, N-1):
#       h_truth.SetBinContent(i, scaled_val[i])
   return h_truth


def get_resp_err_ratio(resp, h_truth):
   rtn = h_truth.Clone(randstr(5))
   err_2D = resp.GetSumw2()
   for i in range(resp.GetNbinsX() + 2):
       b = resp.GetBin(i, i)
       v = resp.GetBinContent(b)
       if v != 0:
          rtn.SetBinContent(i, np.sqrt(err_2D.At(b))/v)
          #dividing an error with bin_val does not make sense to have an error? Maybe can do add an upper and lower error later
          rtn.SetBinError(i, 0)
       else:
          rtn.SetBinContent(i, 0)
   return rtn


def get_resp_err_ratio_2D(resp):
   """find the ratio of err / entry"""
   new = resp.Clone(randstr(5))
   err_2D = resp.GetSumw2()
   for i in range(resp.GetNbinsX() + 2):
      for j in range(resp.GetNbinsX() + 2):
         b = resp.GetBin(i, j)
         v = resp.GetBinContent(b)
         if v != 0:
            new.SetBinContent(b, round(np.sqrt(err_2D.At(b))/v, 2))
         else:
            new.SetBinContent(b, 0)
   return new


def sliding_window(bin_content, weight, size):
   weight = weight[4:]
   #even case
   if size%2 == 0:
      offset = size / 2
   else:
      offset = size // 2
   N = len(bin_content)
   assert( len(bin_content) == len(weight) )
  # for i in range(N):
  #     if i == 0 or i == 1 or i == N - 1 or i == N - 2:
  #        continue
  #     pre  = bin_content[i-1]*weight[i-1]
  #     curr = bin_content[i]*weight[i]
  #     nex  = bin_content[i+1]*weight[i+1]
  #     sumW = weight[i-1] + weight[i] + weight[i+1]
 #      bin_content[i] = (pre + curr + nex)/sumW
   for i in range(N):
      if i < offset or i > N - 1 - offset: continue
      val_window = bin_content[i:i+size]
      weight_win = weight[i:i+size]
      sumW = sum(weight_win)
      bin_content[i] = sum(val_window*weight_win)/sumW

   return bin_content



def gauss_kernel(histo, sigma, is2D, cleaned = False):
      """1D gauss kernel smoothing for 1D distributions"""
      bin_val = np.array(histo)
      if not cleaned and not is2D:
          bin_val = bin_val[5:-1]
      elif cleaned and not is2D:
          bin_val = bin_val[1:-1]
      #bin_centre = np.zeros(histo.GetNbinsX())
      bin_centre = np.zeros(len(bin_val))
      for i in range(1, len(bin_val) + 1):
          bin_centre[i-1] = histo.GetBinCenter(i-1)
      kernel = np.exp(-0.5 * ((bin_centre[:, np.newaxis] - bin_centre)/sigma) ** 2)
      smooth = bin_val*kernel
      smoothed = smooth.sum(axis=1) / kernel.sum(axis=1)
      for i in range(len(smoothed)):
         histo.SetBinContent(i+1, smoothed[i])
     # histo.SetName(randstr(5))
      return histo



def applyWMA(hist):
    """Applies weighted moving average to a 2D histogram."""
    N = hist.GetNbinsX() # number of bins
    # construct 2D numpy array for corresponding to histo bin content
    yvals = np.zeros((N,N))
    weights = np.zeros((N,N)) # weight is the inverse square of the relative stats error
    for icol in range(N):
        for irow in range(N):
            b = hist.GetBin(irow+1, icol+1)
            val = hist.GetBinContent(b)
            if val < 0.:  val = 0.
            err = hist.GetBinError(b)
            yvals[irow][icol] = val
            if err:
                weights[irow][icol] = (val / err) ** 2
    # weights should sum to unity
    weights /= weights.sum()
    weights[weights == 0] = 1.0 # prevents div-by-zero error
    kernel = np.ones((2,2)) # use 2-by-2 kernel to work out moving means
    means = sp.convolve2d(in1=yvals, in2=kernel, mode='same', fillvalue=0)
    counts = sp.convolve2d(in1=np.ones((N,N)), in2=kernel, mode='same', fillvalue=0)
    means /= counts
    # calculate inverse weights for the weighted average
    antiweights = 1.0 / weights
    antiweights /= antiweights.sum()
    #print('yvals', np.diag(yvals))
    #print('means', np.diag(means))
    #print('weights', np.diag(weights))
    #print('anti-weights', np.diag(anti_weights))
    # perform weighted average
    smooth = (yvals * weights + means * antiweights) / (weights + antiweights)
    #print('smooth', np.diag(smooth))
    # update central values and scale errors of original histo
    errs = hist.GetSumw2()
    for icol in range(N):
        for irow in range(N):
            b = hist.GetBin(irow+1, icol+1)
            hist.SetBinContent(b, smooth[irow][icol])
            sf = smooth[irow][icol] / yvals[irow][icol] if yvals[irow][icol] else 0.0
            #print(yvals[irow][icol], np.sqrt(errs.At(b)), sf, '->', hist.GetBinContent(b), np.sqrt(errs.At(b)*sf*sf))
            errs.AddAt(errs.At(b)*sf*sf, b)
    return hist



def plot2D_ratio(hist, smoothed, truth, hname, ch, obs, logx = 0, logy = 0):
    N = hist.GetNbinsX()
    for col in range(N+2): #N+2
       pre_smooth = truth.Clone(randstr(5))
       aft_smooth = truth.Clone(randstr(5))
       for row in range(N+2):
          b = hist.GetBin(row, col)
          pre_smooth.SetBinContent(row, hist.GetBinContent(b))
          aft_smooth.SetBinContent(row, smoothed.GetBinContent(b))
       ratioPlot1D(pre_smooth, aft_smooth, "col = " + str(col) + hname, ch, obs, resp = True )
       pre_smooth.Delete()
       aft_smooth.Delete()

    for row in range(N+2):
       pre_smooth = truth.Clone(randstr(5))
       aft_smooth = truth.Clone(randstr(5))
       for col in range(N+2):
          b = hist.GetBin(row, col)
          pre_smooth.SetBinContent(row, hist.GetBinContent(b))
          aft_smooth.SetBinContent(row, smoothed.GetBinContent(b))
       ratioPlot1D(pre_smooth, aft_smooth, "row = " + str(row) + hname, ch, obs, resp = True)#numor / denom i.e. aft / pre
       pre_smooth.Delete()
       aft_smooth.Delete()
    return



def fluctGauss(hist, rand):
  """Return a fluctuated clone of the histogram
     using a Gaussian-distributed random variable."""
  rtn = hist.Clone(randstr(5))
  oldErr = hist.GetSumw2()
  newErr = rtn.GetSumw2()
  for i in range(hist.GetNbinsX()+2):
    for j in range(hist.GetNbinsY()+2):
        b = hist.GetBin(i,j)
        oldVal = hist.GetBinContent(b)
        if oldVal < 0: oldVal = 0.0
        fluctVal = rand.Gaus(oldVal, hist.GetBinError(b)) if oldVal else 0.0
        if fluctVal < 0: fluctVal = 0.0
        sf = fluctVal / oldVal if oldVal else 0.0
        rtn.SetBinContent(b, fluctVal)
        newErr.AddAt(oldErr.At(b)*sf*sf, b)
  return rtn



def apply_lumi_uncertainty(*args, is2D=False):
    for hist in args:
       err = hist.GetSumw2()
       for i in range(hist.GetNbinsX() + 2):
           for j in range(hist.GetNbinsY() + 2):
               b = hist.GetBin(i, j)
               val = hist.GetBinContent(b)
               err.AddAt(val, i)


def fluctPoisson(hist, rand):
  """Return a fluctuated clone of the histogram
     using a Poisson-distributed random variable."""
  rtn = hist.Clone()
  oldErr = hist.GetSumw2()
  newErr = rtn.GetSumw2()
  for i in range(hist.GetNbinsX()+2):
    for j in range(hist.GetNbinsY()+2):
        b = hist.GetBin(i, j)
        oldVal = hist.GetBinContent(b)
        if oldVal < 0:  oldVal = 0.0
        fluctVal = float(rand.PoissonD(oldVal)) if oldVal else 0.0
        if fluctVal < 0.0: fluctVal = 0.0
        sf = fluctVal / oldVal if oldVal else 0.0
        rtn.SetBinContent(b, fluctVal)
        newErr.AddAt(oldErr.At(b)*sf*sf, b)
  return rtn

def findRatio(data, reco):
    weights = [  ]
    for i in range(data.GetNbinsX()+2):
        if reco.GetBinContent(i) != 0: 
            w = round(data.GetBinContent(i)/reco.GetBinContent(i), 5)
        else:
            w = 1.
        weights.append(w)
    print(weights)
   # print("Fattori per il ripesaggio del Monte-Carlo: {0}".format(weights))
    return weights


def rw_function(ratio, h_data, mode='linear', start=1, end=-1):
   x_axis = [ ]
   for i in range(h_data.GetNbinsX()+2):
       x_axis.append(h_data.GetBinCenter(i))
   x_axis = x_axis[start:end+1]
   y_axis = ratio[start:end+1]
   print(x_axis)
   if mode == 'linear':
      beta, c = np.polyfit(x_axis, y_axis, 1)
      return beta, c
   elif mode == 'poly3': 
      beta, beta2, beta3, c = np.polyfit(x_axis, y_axis, 3)
      return beta, beta2, beta3, c


def GetBSM_signal(INT, BSM, Energy=float, channel=str, region=str):
   map_name = region + '_' + '_'.join(str(Energy).split('.')) + '_g2'
   if region == 'INC':
      map_dict = dta_labels.g2_map[map_name]
      g2 = map_dict[channel]
      INT_scale = g2
      BSM_scale = g2**2
      print(f"g value used for this BSM injection is {g2} and g^2 for INT is {INT_scale}, g^4 for BSM is {BSM_scale}")
      INT.Scale(INT_scale)
      BSM.Scale(BSM_scale)
      rtn = INT.Clone(randstr(5))
      rtn.Add(BSM)
      return rtn

   bjets = ['0bjet', '1bjet', '2bjet']
   for b in range(bjets):
      map_dict = bjets[b] + '_' + map_name
      g2 = map_dict[channel]
      INT_new = INT.Clone(randstr(5))
      BSM_new = BSM.Clone(randstr(5))
      INT_new.Scale(g2**2)
      BSM_new.Scale(g2**4)
      if b == 0:
         for i in range(16, 30):
            INT.SetBinContent(i-16, INT_new.GetBinContent(i))
            BSM.SetBinContent(i-16, BSM_new.GetBinContent(i))
      elif b == 1:
         for i in range(31, 45):
            INT.SetBinContent(i-16, INT_new.GetBinContent(i))
            BSM.SetBinContent(i-16, BSM_new.GetBinContent(i))
      else:
         for i in range(46, 60):
            INT.SetBinContent(i-16, INT_new.GetBinContent(i))
            BSM.SetBinContent(i-16, BSM_new.GetBinContent(i))
   rtn = INT.Clone(randstr(5))
   rtn.Add(BSM)
   return rtn


def get_error_binning(hist):
   N = hist.GetNbinsX() + 2
   binning = []
   for i in range(N):
      binning.append(hist.GetBinLowEdge(i))
   return binning



def Get_bias(unfolded, truth):
   """find the ratio bias (ratio diff)"""
   no_bins = unfolded.GetNbinsX() + 2
   res = unfolded.Clone(randstr(5))
   tru = truth.Clone(randstr(5))
   res.Add(tru, -1)
   res.Divide(tru)
   tru.Delete()
   return res


def find_sigma(SM, BSM):
   n = SM.GetNbinsX() + 2
   sigma_bins = np.zeros(n)
   sigma_bins_weighted = np.zeros(n)
   std_err = np.zeros(n)
   top_5_weighted_sigma = [ ]
   top_5_abs_sigma = [ ]
   no_greater_3sig = 0
   for i in range(n):
      sigma = SM.GetBinError(i)
      BSM_val = BSM.GetBinContent(i)
      SM_val = SM.GetBinContent(i)
      diff = BSM_val - SM_val
      sigma_bins[i] = diff/sigma if sigma else 0.0
      if sigma_bins[i] >= 3.0: no_greater_3sig += 1
      std_err[i] = 1/np.sqrt(SM_val) if SM_val > 0 else 0.0
   total_sigma, total_weight = sum(sigma_bins), sum(std_err)
   sigma_bins_weighted = [ sigma_bins[m]/std_err[m] if std_err[m] else 0.0 for m in range(n) ]
   res = sum(sigma_bins_weighted)/total_weight
     # sigma_bins[i] = no_of_sigma**2
   #find top 5 sigmas
   sigma_bins = list(sigma_bins)
   sigma_bins_weighted = list(sigma_bins_weighted)
   heapq.heapify(sigma_bins)
   heapq.heapify(sigma_bins_weighted)
   for i in range(n - 5):
       heapq.heappop(sigma_bins_weighted)
       heapq.heappop(sigma_bins)
   print(f"the top 5 std error weighted injections levels are {sigma_bins_weighted}")
   print(f"the top 5 absolute injections levels are {sigma_bins}")
   print(f"the number of points with > 3 sigma injections are {no_greater_3sig}")
   return res
#   return np.sqrt(sum(sigma_bins))



def get_edges(histo):
  """get the the bins"""
  return np.array([ histo.GetXaxis().GetBinUpEdge(i) for i in range(histo.GetNbinsX()+1) ])


def update_error(histo, SYST_MAP, filt_expr = ''):
  """sum the errors that is current there"""
  total = np.zeros(len(SYST_MAP['']))
  vals = [ histo.GetBinContent(i) for i in range(histo.GetNbinsX()+2) ]
  for syst, errs in SYST_MAP.items():
    if not syst or syst == 'edges' or filt_expr not in syst:
      continue
    if '__1down' in syst:
      continue
    if '__1up' in syst:
      total += (0.5 * (errs - SYST_MAP[syst.replace('__1up', '__1down')])) ** 2
      continue
    total += errs
  rel_err = safeDiv(np.sqrt(total), SYST_MAP[''])
  total = (rel_err * vals) ** 2
  herrSq = histo.GetSumw2()
  for i in range(len(total)):
    herrSq.AddAt(total[i], i)



def printRel(SYST_MAP, sf = 1.0):
  totSq = np.zeros(len(SYST_MAP['']))
  for syst, errs in SYST_MAP.items():
    if not syst or syst == 'edges':
      continue
    rel_err = sf * safeDiv(np.sqrt(errs), SYST_MAP[''])
    print ('relError', syst, rel_err)


def relError(TOSKIP, SYST_MAP, sf = 1.0, filt_expr = ''):
  """Take the dynamic list of negligible systs into consideration
     and output a reduced list as well as an original error curve.
     This is the function where the real errors are propagated for
     systematic uncertainties, the stats and modelling uncertainties
     are calculated on-the-fly within each sub-sections in the DTA-unfold."""
  edges = SYST_MAP['edges']
  rtn_all = rt.TH1F(randstr(5) + '_rel_err' + filt_expr, '', len(edges) - 1, edges)
  rtn_cut = rt.TH1F(randstr(5) + '_rel_err' + filt_expr, '', len(edges) - 1, edges)
  totSq_all = np.zeros(len(SYST_MAP['']))
  totSq_cut = np.zeros(len(SYST_MAP['']))
  for syst, errs in SYST_MAP.items():
    if not syst or syst == 'edges' or filt_expr not in syst:
      continue
    if filt_expr and not syst.startswith(filt_expr): # there's an "EL_" in "MODEL_", d'oh
      continue
    if '__1down' in syst:
      continue
    if '__1up' in syst:
      #print (syst, 100.*safeDiv(0.5 * (errs - SYST_MAP[syst.replace('__1up', '__1down')]), SYST_MAP['']))
      totSq_all += (0.5 * (errs - SYST_MAP[syst.replace('__1up', '__1down')])) ** 2
      if not syst.split('__')[0] in TOSKIP:
         totSq_cut += (0.5 * (errs - SYST_MAP[syst.replace('__1up', '__1down')])) ** 2
      continue
    #print (syst, 100.*safeDiv(np.sqrt(errs), SYST_MAP['']))
    totSq_all += errs
    totSq_cut += errs
  rel_err_all = sf * safeDiv(np.sqrt(totSq_all), SYST_MAP[''])
  rel_err_cut = sf * safeDiv(np.sqrt(totSq_cut), SYST_MAP[''])
  for i in range(rtn_all.GetNbinsX() + 2):
    rtn_all.SetBinContent(i, rel_err_all[i])
    rtn_cut.SetBinContent(i, rel_err_cut[i])
  return rtn_all, rtn_cut



def calculate_CI(histo, CL):

      mean =    histo.GetMean()
      std =     histo.GetStdDev()
      std_err = histo.GetStdDevError()
      N =       histo.GetNbinsX()
      CI_up = mean + CL*(std/np.sqrt(N))
      CI_dn = mean - CL*(std/np.sqrt(N))
      return CI_up, CI_dn


def calculate_p_val(curves, reduced, ERROR_TYPES):
     """calculate the p val between the reduced error curve vs the origin error curve and convert to CL"""
     res = { }
     N_bins = curves[0].GetNbinsX()
     for i in range(len(curves)):
         chi2_sum = 0
         for j in range(N_bins + 2):
             red_val = reduced[i].GetBinContent(j)
             nom_val = curves[i].GetBinContent(j)
             if nom_val == 0:
                continue
             else:
                diff2 = (red_val - nom_val) ** 2
                chi2_sum += diff2/(nom_val)
         p_val = 1 - stats.chi2.cdf(chi2_sum, N_bins)
         res[ERROR_TYPES[i][0]] = str(p_val*100) + "%"
     return res

#not in use at the moment, calculate p_val is probably better
def calculate_CL(curves, reduced, ERROR_TYPES):
     res_nom = [ ]
     res_red = [ ]

     CLs = { }
     sqrtN = np.sqrt(curves[0].GetNbinsX())
     for i in range(len(curves)):
         mean = curves[i].GetMean()
         std = curves[i].GetStdDev()
         diff = abs(mean - reduced[i].GetMean())
         CL = (diff * sqrtN) / std
         if CL == 0: #or CL > 1:
            CL = 1
         CLs[ERROR_TYPES[i][0]] = str(100*CL) + "%"
#     for i in range(len(res_nom)):
#         res_nom[i].Delete()
#         res_red[i].Delete()
     return CLs


def find_chi2(mc1, mc2, var, level, weighted=False):
   N = mc1.GetNbinsX() + 2
   diff = np.zeros(N)
   for i in range(N):
       ref_val = mc1.GetBinContent(i)
       diff_val = mc2.GetBinContent(i) - ref_val
       #err = np.sqrt(mc1.GetBinContent(i))
       diff[i] = diff_val**2/ref_val if ref_val else 0
       if weighted:
          diff[i] /= np.sqrt(ref_val) if ref_val else 1
   chi2_sum = sum(diff)
   p_val = 1 - stats.chi2.cdf(chi2_sum, N)
   print(f"chi2 value for the {var} variance to sherpa at {level} level is {chi2_sum} with a corresponding p-val {p_val}")



def rebin2D(h_old, xbins, ybins = None):
   """utilily function to rebin any 2D histogram with specific binnings"""
   if not ybins: ybins = xbins
   xaxis = h_old.GetXaxis()
   yaxis = h_old.GetYaxis()
   e_old = h_old.GetSumw2()
   h_new = rt.TH2F(str(h_old), 'rebinned_migration_matrix', len(xbins)-1, array('d',xbins), len(ybins)-1, array('d',ybins))
#   print(xaxis.GetNbins())
#   print(yaxis.GetNbins())
   e_New = h_new.GetSumw2()
   for i in range(xaxis.GetNbins()+2):
     for j in range(yaxis.GetNbins()+2):
        x_old = xaxis.GetBinCenter(i)
        y_old = yaxis.GetBinCenter(j)
        bin_old = h_old.GetBin(i,j)
        bin_new = h_new.FindBin(x_old,y_old)
        newVal = h_new.GetBinContent(bin_new) + h_old.GetBinContent(bin_old)
        if newVal < 0.:  newVal = 0.
        h_new.SetBinContent(bin_new, newVal)
        e_New.AddAt(e_New.At(bin_new)+e_old.At(bin_old) if newVal else 0., bin_new)
   return h_new


def show_overflow(hist, label, binningX, binningY = None):
    mod = None
    if binningY:
        mod = rt.TH2F(randstr(5), randstr(5),
                      len(binningX)-1, binningX,
                      len(binningY)-1, binningY)
        olderrs = hist.GetSumw2()
        moderrs = mod.GetSumw2()
        for i in range(hist.GetNbinsX()+2):
            for j in range(hist.GetNbinsY()+2):
                bold = hist.GetBin(i,j)
                bnew = mod.GetBin(i,j)
                mod.SetBinContent(bnew, hist.GetBinContent(bold))
                moderrs.AddAt(olderrs.At(bold), bnew)
        mod.SetMarkerColor(hist.GetMarkerColor())
        mod.SetLineColor(hist.GetLineColor())
        mod.GetXaxis().ChangeLabel(-1,-1,-1,-1,-1,-1,label)
        mod.GetYaxis().ChangeLabel(-1,-1,-1,-1,-1,-1,label)
    else:
        mod = rt.TH1F(randstr(5), randstr(5), len(binningX)-1, binningX)
        olderrs = hist.GetSumw2()
        moderrs = mod.GetSumw2()
        for i in range(hist.GetNbinsX()+2):
            mod.SetBinContent(i, hist.GetBinContent(i))
            moderrs.AddAt(olderrs.At(i), i)
        mod.SetMarkerColor(hist.GetMarkerColor())
        mod.SetLineColor(hist.GetLineColor())
        mod.GetXaxis().ChangeLabel(-1,-1,-1,-1,-1,-1,label)
    hist.Delete()
    return mod


def binWidthDiv(*histos, factor = 1.0):
  for histo in histos:
      err = histo.GetSumw2()
      for i in range(1, histo.GetNbinsX() + 1):
        sf = factor / histo.GetBinWidth(i)
        histo.SetBinContent(i, sf * histo.GetBinContent(i))
        err.AddAt(err.At(i) * sf * sf, i)


def binWidthMult(*histos, factor = 1.0):
  for histo in histos:
      err = histo.GetSumw2()
      for i in range(1, histo.GetNbinsX() + 1):
        sf = histo.GetBinWidth(i) / factor
        histo.SetBinContent(i, sf * histo.GetBinContent(i))
        err.AddAt(err.At(i) * sf * sf, i)



def rownorm2D(histo2D, target = 1.0):
  err = histo2D.GetSumw2()
  nBins = histo2D.GetNbinsX()
  for i in range(nBins + 2):
    area = sum([ histo2D.GetBinContent(j, i) for j in range(nBins + 2) ])
    sf = target / area if area else 0.0
    for j in range(1, nBins + 1):
      b = histo2D.GetBin(j, i)
      histo2D.SetBinContent(b, sf * histo2D.GetBinContent(b))
      err.AddAt(err.At(b) * sf * sf, b)



def colnorm2D(histo2D, target = 1.0):
  err = histo2D.GetSumw2()
  nBins = histo2D.GetNbinsX()
  for i in range(nBins + 2):
    area = sum([ histo2D.GetBinContent(i,j) for j in range(nBins + 2) ])
    sf = target / area if area else 0.0
    for j in range(1, nBins + 1):
      b = histo2D.GetBin(i, j)
      histo2D.SetBinContent(b, sf * histo2D.GetBinContent(b))
      err.AddAt(err.At(b) * sf * sf, b)



def makeSkeleton(graph):
    edges = np.array([])
    x = ctypes.c_double(0.)
    y = ctypes.c_double(0.)
    for i in range(graph.GetN()):
      graph.GetPoint(i, x, y)
      xlo = graph.GetErrorXlow(i)
      xhi = graph.GetErrorXhigh(i)
      if not i:  edges = np.append(edges, np.float32(x) - xlo)
      edges = np.append(edges, np.float32(x) + xhi)
    return rt.TH1F(graph.GetName() + '_skel', '', len(edges) - 1, edges)



def get_channel(name):
    if not name:
      return ''
    isSameSign = 'SSs' in name
    rtn = ', pp #rightarrow #tau^{#pm}_{%s}#tau^{%s}_{%s}'
    return rtn % (name[-2].lower(), '#pm' if isSameSign else '#mp', name[-1].lower())




def get_ratio(h_unfolded, v_unfolded, zoom = False):
  """find the ratio between the nominal unfolded and variation unfolded"""
  if not zoom:
     new = h_unfolded.Clone() #randstr(5))
  else:
     edges = get_edges(h_unfolded)[:30]
     new = rt.TH1F(randstr(5) + "unfolded ratio",'' , len(edges) - 1, edges)
  for i in range(new.GetNbinsX() + 2):
     if h_unfolded.GetBinContent(i) <= 0 or v_unfolded.GetBinContent(i) <= 0:
        new.SetBinContent(i, 0)
     else:
        new.SetBinContent(i, h_unfolded.GetBinContent(i)/v_unfolded.GetBinContent(i))
  return new


def apply_syst_scale(nom, ratio):
   """apply the scale of the sub_nominal syst vs nomianl onto the real won nominal"""
   new = nom.Clone(randstr(5))
   for i in range(new.GetNbinsX() + 2):
     if new.GetBinContent(i) <= 0 or ratio.GetBinContent(i) <= 0:
        new.SetBinContent(i, 0)
     else:
        new.SetBinContent(i, nom.GetBinContent(i)*ratio.GetBinContent(i))
   return new



def passSystSignificance(systs, dn_systs, stat): #compare_to_stats(syst, stats):
    """compare the systematic significance with respect to stats"""
#    return any([ sy / st > 0.5 for sy, st in  zip(systs,stats) if st])
 ##   stat = (1 / np.sqrt(np.array(nom)))**2
    for l in range(len(systs)):
        #print((0.5 * (systs[l] - dn_systs[l])) ** 2)
        if stat[l] == 0 or systs[l] == 0:
           continue
        else:
           diff = (0.5 * (systs[l] - dn_systs[l])) ** 2
           #diff = 0.5 * ((systs[l] - nom[l]) ** 2 + (dn_systs[l] - nom[l]) ** 2)
           #print("sss", l, diff/stat[l])
           #or using systs[l] - nom[l] ** 2 ?
           if diff / stat[l] > 0.01:
           #   print("ttt", l, diff/stat[l])
              return True
    return False



def get_syst_eff_ranking(syst_dict):#, percent):
    """output a queue for the chi2-test sorted rank"""
    rank = [ ]
    res = collections.deque()
    for syst_set in syst_dict:
      for i in range(len(syst_dict[syst_set])):
          var, ratio = syst_dict[syst_set][i][0], syst_dict[syst_set][i][1]
          rank.append((var, get_syst_eff(ratio)))
    rank.sort(key = lambda x: x[1])
    for t in rank:
       res.append(t[0])
   # return np.percentile(rank, percent)
    return res


def add_hist(first, nom_name, scalings, files, model):
  # new_first = first.Clone(randstr(5))
  # first.Delete()
  # new_first.Scale(scalings[model][0])
   for i in range(1, len(files[model])):
      tmp_nom = files[model][i].Get(nom_name)
      tmp_nom.Scale(scalings[model][i])
      first.Add(tmp_nom)
      tmp_nom.Delete()
   return first


def get_syst_eff(ratio):
    """chi2 test for each systematic ratio wrt nominal"""
    res = 0
    for i in range(ratio.GetNbinsX() + 2):
        bin_val = ratio.GetBinContent(i)
        bin_error = ratio.GetBinError(i)
        if bin_val <= 0:
           error_ratio = 0
        else:
           error_ratio = bin_error / bin_val
        if error_ratio == 0:
           diff = 0
        else:
           diff = ((bin_val - 1) / error_ratio)**2
        res += diff
    rtn = np.sqrt(res)
    return rtn


def toy_example():

    # detector simulation
    def smear(xt):
      xeff = 0.3 + (1.0 - 0.3) / 20 * (xt + 10.0)  # efficiency
      x = rt.gRandom.Rndm()
      if x > xeff:
        return None
      xsmear = rt.gRandom.Gaus(-2.5, 0.2)  # bias and smear
      return xt + xsmear

    # create some dummy MC
    nbins = 40;  binLo = -10.0;  binHi =  10.0
    mc_rec = rt.TH1F('mc_reco', 'MC reco',  nbins, binLo, binHi)
    mc_tru = rt.TH1F('mc_truth', 'MC truth', nbins, binLo, binHi)
    mc_res = rt.TH2F('mc_response', 'MC migration', nbins, binLo, binHi, nbins, binLo, binHi)
    nMC = 100000
    for i in range(nMC):
      xt = None
      xr = rt.gRandom.Uniform()
      if xr < 0.05:
        # "fakes" (an event with no truth equivalent)
        xr = rt.gRandom.Uniform(-10.0, 10.0)
      else:
        xt = rt.gRandom.BreitWigner(0.3, 2.5)
        xr = smear(xt)

      if xt != None and xr != None:
        # pass both truth and reco
        mc_rec.Fill(xr)
        mc_tru.Fill(xt)
        mc_res.Fill(xr, xt)
      elif xt != None:
        # pass truth only
        mc_tru.Fill(xt)
      else:
        # pass reco only
        mc_rec.Fill(xr)

    # now generate some dummy data
    hPDF  = rt.TH1D('pdf',  'model', nbins, binLo, binHi)
    hData = rt.TH1D('data', 'data',  nbins, binLo, binHi)
    #  Test with a Gaussian, mean 0 and width 2.
    nData = 10000
    for i in range(10000):
      xt = None
      xr = rt.gRandom.Uniform()
      if xr < 0.05:
        # events with no truth equivalent
        xr = rt.gRandom.Uniform(-10.0, 10.0)
      else:
        xt = rt.gRandom.Gaus(0.0, 2.0)
        xr = smear(xt)

      if xt != None:  hPDF.Fill(xt)
      if xr != None:  hData.Fill(xr)

    nIter = 4
    hUnfold = IBUnfold(hData, mc_rec, mc_tru, mc_res, nIter)

    # plot the histograms
    CANVAS = rt.TCanvas('c', 'global canvas', 800, 600)
    CANVAS.SetBottomMargin(0.15)

    hUnfold.SetMarkerSize(0.8)
    hUnfold.SetMarkerColor(rt.kAzure)
    hUnfold.SetLineColor(rt.kAzure)
    hUnfold.GetXaxis().SetTitle('observable')
    hUnfold.GetYaxis().SetTitle('Events')
    hUnfold.Draw()

    hData.SetMarkerSize(0.8)
    hData.SetMarkerColor(rt.kRed)
    hData.SetLineColor(rt.kRed)
    hData.Draw('same')

    hPDF.SetMarkerSize(0.8)
    hPDF.SetMarkerColor(8)
    hPDF.SetLineColor(8)
    hPDF.Draw('same')

    leg = rt.TLegend(0.18, 0.80, 0.30, 0.92)
    leg.SetFillStyle(0)
    leg.SetTextColor(1)
    leg.SetTextFont(42)
    leg.SetTextSize(0.04)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    marker = 'LE'
    leg.AddEntry(hUnfold, 'unfolded data',    marker)
    leg.AddEntry(hData,   'measured data',    marker)
    leg.AddEntry(hPDF,    'underlying model', marker)
    leg.Draw('same')

    CANVAS.Print('dta_unfolding_test.pdf')
    CANVAS.Close()


def rebin2D(hOld, xbins, ybins):
   if not ybins:  ybins = xbins
   xaxis = hOld.GetXaxis()
   yaxis = hOld.GetYaxis()
   eOld  = hOld.GetSumw2()
   hNew  = rt.TH2F(str(hOld) + '_rebinned', hOld.GetTitle(),
                   len(xbins) - 1, xbins, len(ybins) - 1, ybins)
   eNew  = hNew.GetSumw2()
   for i in range(xaxis.GetNbins() + 2):
     for j in range(yaxis.GetNbins() + 2):
       xold = xaxis.GetBinCenter(i)
       yold = yaxis.GetBinCenter(j)
       bold = hOld.GetBin(i, j)
       bnew = hNew.FindBin(xold, yold)
       hNew.SetBinContent(bnew, hNew.GetBinContent(bnew) + hOld.GetBinContent(bold))
       eNew.AddAt(eNew.At(bnew) + eOld.At(bold), bnew)
   return hNew


def softDivide(numer, denom):
    errs = numer.GetSumw2()
    for i in range(numer.GetNbinsX() + 2):
        n = numer.GetBinContent(i)
        d = denom.GetBinContent(i)
        sf = 1.0 / d if d else 1.0
        numer.SetBinContent(i, n*sf)
        errs.AddAt(errs.At(i)*sf*sf, i)

def ptllRW(data, reco, ratio_fit_check, param_list):
   Nbins = data.GetNbinsX() + 2
   for nbins in range(Nbins):       
      ptll_val = data.GetBinCenter(nbins)
      data_bin = data.GetBinContent(nbins)
      reco_bin = reco.GetBinContent(nbins)     
      bin_up_edge= data.GetBinLowEdge(nbins)+ data.GetBinWidth(nbins)
      ratio_fit = 0
      if (reco_bin <= 0) or (data_bin <= 0):
         continue
      elif bin_up_edge > 160.0: #for last four points fit pol1
      #Set the fitted parameters
         for nparam in range(len(param_list)):
            ratio_fit += param_list[nparam] * pow(ptll_val, nparam)
      else: #do bin-2-bin RW
         ratio_fit = data_bin/reco_bin
         #PRINT OUT THE BIN2BIN fitted results
      ratio_fit_check.SetBinContent(nbins, data_bin/(reco_bin * ratio_fit))
   return ratio_fit_check

   
def RW(data, reco, ratio_fit_check, param_list, start=1, end=-1):
   Nbins = data.GetNbinsX() + 2
   for b in range(start, Nbins):
       if b > end: break
       data_val = data.GetBinContent(b)
       reco_val = reco.GetBinContent(b)
       mll      = data.GetBinCenter(b)
       ratio_fit = 0
       for param in range(len(param_list)):
           ratio_fit += param_list[param] * pow(mll, param)
       if data_val <= 0 or reco_val <= 0: continue
       ratio_fit_check.SetBinContent(b, data_val/(reco_val * ratio_fit))
   return ratio_fit_check  










      
