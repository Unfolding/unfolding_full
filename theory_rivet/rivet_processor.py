import rivet
import helperfunction as hf
import os
#1. merge raw rivet directories into on file
#2. scale each process
#3. stack corresponding processes together into processes SH_Drell_Yan, SH_TTBAR, SH_TOP, PP8_Drell_Yan, PP8_TTBAR, etc. 

if not os.path.isdir('merged_rivet_inputs') or not os.listdir('merged_rivet_inputs'):
   hf.merge_raw_rivet()
else: print('merged_rivet_inputs directory is not empty, skipping raw rivet file merging ...')

if not os.path.isdir('scaled_rivet_inputs') or not os.listdir('scaled_rivet_inputs'):
   hf.scale_merged_rivet()
else: print('scaled_rivet_inputs directory is not empty, skipping rivet file scaling ...')

if not os.path.isdir('stacked_processes') or not os.listdir('stacked_processes'):
   hf.stack_to_processes()
else: print('stacked_processes directory is not empty, please make sure the stacked processes files are to be removed before rerun the process')
