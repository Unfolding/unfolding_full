import glob
import os
import collections

pmg = "/unix/bsmxsec3/tyue/final_rivet/dta_pmg_xsec_db-2023-11-22.log"

def find_unmatch_DSID(merged_dir="merged_rivet_inputs", raw_input_dir="raw_rivet_output"):
   matching_dict = {}
   for f in os.listdir(raw_input_dir):
       if f.endswidth('.yoda'):
          #DSID = f.split['_'][2]
          matching_dict[f] = 1
   for f in os.listdir(merged_dir):
       if f.endswith('.yoda'):
          #DSID = f.split('_')[0]
          matching_dict[f] += 1
   missing_files = [ ]
   for key, value in matching_dict.items():
       if value == 1:
          missing_files.append(key)
  # missing_DSID = second_set - first_set

   return missing_files 


def merge_raw_rivet(input_dir="raw_rivet_output"):
   if not os.path.isdir(input_dir): 
      raise Exception("no rivet raw input file found!") 
   files = glob.glob(input_dir + "/*")
   if not os.path.isdir('merged_rivet_inputs/'): os.mkdir('merged_rivet_inputs/')
   if os.listdir('merged_rivet_inputs'): files = find_unmatch_DSID()
   for f in files:
      print(f)
      DSID, name = f.split('.')[2:4]
     # cmd = "yodamerge " + f + '/* -o ' + 'merged_rivet_inputs/' + DSID +'_' + name + ".yoda"
      cmd = "rivet-merge -e " + f + '/* -o ' + 'merged_rivet_inputs/' + DSID +'_' + name + ".yoda"
      os.system(cmd)


def scale_merged_rivet(input_dir="merged_rivet_inputs"):
    if not os.path.isdir(input_dir):
       raise Exception("no merged raw input file directory found, please run merge_raw_rivet()")
    files = glob.glob(input_dir + "/*")
    if os.listdir('scaled_rivet_inputs'): files = find_unmatch_DSID(merged_dir='scaled_rivet_inputs', raw_input_dir="merged_rivet_inputs")
    scale_factors = {}
    procs = {}
    with open(pmg, 'r') as f:
       for row in f.readlines()[1:]:
          DSID, short, xs, fe, _, _, _, _, etag = row.strip().split('\t\t')
          #if DSID == '700458': print(xs, fe)
          scale_factors[DSID] = float(xs)*float(fe)
    if not os.path.isdir('scaled_rivet_inputs'): os.mkdir('scaled_rivet_inputs')
    for f in files:
       name_split = f.split('/')[1].split('_')
       DSID = name_split[0]
       name = f.rstrip(".yoda").split('/')[-1]
       cmd = "rivet-merge " + f + ":" + str(scale_factors[DSID]) + " -o scaled_rivet_inputs/scaled_" + name + ".yoda"
      # cmd = "yodastack " + f + ":" + str(scale_factors[DSID]) + " -o scaled_rivet_inputs/scaled_" + name + ".yoda"
       #if not proc_name in procs:
       #   procs[proc_name] = []
       #procs[proc_name].append(f)
       os.system(cmd)


def stack_to_processes(input_dir="scaled_rivet_inputs"):
    if not os.path.isdir(input_dir):
       raise Exception("no scaled rivet input found, please run scale_merged_rivet()")
    process = collections.defaultdict(list)
    files = glob.glob(input_dir + "/*")
    if os.listdir('stacked_processes'): files = find_unmatch_DSID(merged_dir='stacked_processes', raw_input_dir="scaled_rivet_inputs")
    single_top = [410644, 410645, 410658, 410659] 
    for f in files:
       name_split = f.split('/')[1].split('_')
       DSID = int(name_split[1])
       if (301040 <= DSID <= 301058) or DSID == 361108:
          os.system("sed -i 's/ATLAS_STDM_2021_10:PP8=ON:STITCH=ON/ATLAS_STDM_2021_10/g' " + f)
          process['PP8_Drell_Yan'].append(f)
       elif DSID == 700660:
          process['Sherpa_TTBAR'].append(f)
       elif DSID == 410472:
          process['PP8_TTBAR'].append(f)
       elif DSID == 700360:
          process['Sherpa_VJJ'].append(f)
       elif DSID in single_top:
          process['PP8_SingleTop'].append(f)
       elif DSID == 410648 or DSID == 410649:
          process['PP8_WTop'].append(f)
       elif (700458 <= DSID <= 700460):
          process['Sherpa_Drell_Yan'].append(f)
       elif (700373 <= DSID <= 700375):
          os.system("sed -i 's/ATLAS_STDM_2021_10:STITCH=ON/ATLAS_STDM_2021_10/g' " + f)
          process['Sherpa_Drell_Yan'].append(f)
       elif (600017 <= DSID <= 600020):
          process['PH7_singleTop'].append(f) 
    if not os.path.isdir("stacked_processes"): os.mkdir("stacked_processes")
    for proc in process:
#	cmd = "rivet-merge " +  ' '.join(process[proc]) + " -o " + '/unix/cedar/pwang/stacked_processes/' + proc + ".yoda"
       cmd = "rivet-merge " + ' '.join(process[proc]) + " -o " + '/unix/cedar/pwang/stacked_processes/' + proc + ".yoda"
      #  cmd = "yodastack " +  ' '.join(process[proc]) + " -o " + 'stacked_processes/'  + proc + ".yoda"
       
       #print(cmd)
       os.system(cmd)











