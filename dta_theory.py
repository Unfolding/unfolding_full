import numpy as np
import ROOT as rt
from array import array
import yoda
import os
import re
import dta_labels, dta_utils
import lhapdf

#prediction uncertainty implementation for the unfolding framework, codes logic from Peng's rivet uncertainty derivation study
#TODO run all processes in different generator to construct the full asymmetric err band for each process (SM sherpa), (SM pp8)
def find_scales(SCALES, hists, hist_name, up_noms, dn_noms):
    scaleMEPSup = up_noms
    scaleMEPSdn = dn_noms
    for scale in SCALES:
      MEPS_var = np.array([ b.sumW() for b in hists[f"{hist_name}[{scale}]"].bins()])
      scaleMEPSup = np.array(list(map(max, zip(up_noms, MEPS_var))))
      scaleMEPSdn = np.array(list(map(min, zip(dn_noms, MEPS_var))))
    return scaleMEPSup, scaleMEPSdn


def calculate_pdf_uncertainty_manuel(yodafile, lower_range, upper_range, pdf_list):
    hist_name = "/ATLAS_STDM_2021_10/mll"
    
    # Determine the LHAPDF name
    hessian = False
    if lower_range == 303200 and upper_range == 303300:
        pdf_name = 'NNPDF30_nnlo_as_0118_hessian'
        hessian = True
    elif lower_range == 260000 and upper_range == 260100:
        pdf_name = 'NNPDF30_nlo_as_0118'
    else:
        raise ValueError("Unknown PDF range")
    
    # Load the PDF set
    pdf_set = lhapdf.getPDFSet(pdf_name)
    pdf_members = [pdf_set.mkPDF(i) for i in range(pdf_set.size)]
    central_pdf = pdf_members[0]
    nom_hist = yodafile[f"{hist_name}"]
    num_bins = len(nom_hist.bins())
    
    # prepare to gather values for each PDF member
    pdf_hists = []
    if len(pdf_list) + 1 == len(pdf_members):
        pdf_hists.append(nom_hist)
        central_pdf = nom_hist
    
    for h in pdf_list:
        pdf_hists.append(yodafile[f"{hist_name}[{h}]"])
    
    pdf_vals = np.zeros((len(pdf_hists), num_bins+1))
    
    # Populate the values array
    for i, hist in enumerate(pdf_hists):
        pdf_vals[i, :num_bins] = [b.sumW() for b in hist.bins()]
        overflow = hist.overflow().sumW()
        pdf_vals[i, num_bins] = overflow
    bin_widths = np.array([b.xMax() - b.xMin() for b in nom_hist.bins()])
   # last_bin = bin_widths.pop()
   # second_bin = bin_widths.pop()
   # bin_widths = np.append(bin_widths, last_bin+second_bin) 
    bin_widths = np.append(bin_widths, 1.0)
    # Manual uncertainty calculation
    pdf_up = np.zeros(num_bins+1)
    pdf_dn = np.zeros(num_bins+1)
    
    for idx in range(num_bins+1):
        member_values = pdf_vals[:, idx]*1000
        if hessian:
           central_value = member_values[0]
           deviations = (member_values - central_value)
           sq_deviations = deviations ** 2
           uncertainty = np.sqrt(np.sum(sq_deviations)) / bin_widths[idx]
        else:
           central_value = np.mean(member_values)
           uncertainty = np.std(member_values, ddof=1) / bin_widths[idx]
        pdf_up[idx] = uncertainty
        pdf_dn[idx] = uncertainty
        print(f"Bin {idx}:")
        print(f"  errplus  = {pdf_up[idx]}")
        print(f"  errminus = {pdf_dn[idx]}")
        print(f"  central  = {central_value}")
    return pdf_up, pdf_dn



def calculate_pdf_uncertainty(yodafile, lower_range, upper_range, pdf_list):
    hist_name = "/ATLAS_STDM_2021_10/mll"
    #determine the LHAPDF name
    if lower_range == 303200 and upper_range == 303300:
       pdf_name = 'NNPDF30_nnlo_as_0118_hessian'
    elif lower_range == 260000 and upper_range == 260100:
       pdf_name = 'NNPDF30_nlo_as_0118'
    else:
       raise ValueError("Unknown PDF range")
    #find all LHAPDF members
    pdf_set = lhapdf.getPDFSet(pdf_name)
    pdf_members = [pdf_set.mkPDF(i) for i in range(pdf_set.size)]
    nom_hist = yodafile[f"{hist_name}"]
    num_bins = len(nom_hist.bins())
    central_pdf = pdf_members[0]
    #if not pdf_hists or len(pdf_hists) == (upper_range - lower_range) + 1:
    pdf_hists = [ ]   
    #sometime missing 1 count, so adding the nominal in
    if len(pdf_list) + 1 == len(pdf_members):
      # pdf_hists.append([0.0 for _ in range(num_bins)]) 
       pdf_hists.append(nom_hist)
    #make all related histogram in the yodafile into a list
    for h in pdf_list:
       pdf_hists.append(yodafile[f"{hist_name}[{h}]"])
       hist = yodafile.get(f"{hist_name}[{h}]")
       #if hist is not None:
       #   print(f"Histogram {h}:", [b.sumW() for b in hist.bins()])
       #else:
       #   print(f"Histogram {h} not found.")
    pdf_vals  = np.zeros((len(pdf_hists), num_bins))
    #write the vals of all variation hists into a 2D array
    for i, hist in enumerate(pdf_hists):
       pdf_vals[i] = [ b.sumW() for b in hist.bins() ]
    #print(pdf_vals[0])
    #print(len(pdf_vals), pdf_set.size)
    # find the corresponding up, dn uncertainties
  #     uncertainties[idx] = pdf_set.uncertainty(pdf_vals[:, idx])
    uncertainties = [ pdf_set.uncertainty(pdf_vals[:, idx]) for idx in range(num_bins) ]
   # print(f"PDF Set Name: {pdf_set.name}")
   # print(f"Number of PDF Members: {pdf_set.size}")
    #for i, member in enumerate(pdf_members):
    #   print(f"PDF Member {i}, bin 0 value: {pdf_vals[i, 0]}")
    for idx, unc in enumerate(uncertainties):
       print(f"Bin {idx}:")
       print(f"  errplus  = {unc.errplus}")
       print(f"  errminus = {unc.errminus}")
       print(f"  central  = {unc.central}")
    pdf_up = np.array([ pdf.errplus for pdf in uncertainties ])
    pdf_dn = np.array([ pdf.errminus for pdf in uncertainties ])  
   # min_value = np.min(pdf_vals[:, idx])
   # max_value = np.max(pdf_vals[:, idx])
   # print("Min value:", min_value, "Max value:", max_value) 
    return pdf_up, pdf_dn



def scale_uncertainty(yoda_dict):
    '''find scale uncertainty for the processes, return a dictionary with up and down errors in tuple'''
    '''stats uncertainty combined in quadrature'''
  #  hist_name = "/ATLAS_STDM_2021_10:TAU_MODE=VISIBLE/mll"
    hist_name = "/ATLAS_STDM_2021_10/mll"
    res_dict = {}
    unit_scale = 1000
    #iteration through all yoda input
    for proc_name, f in yoda_dict.items():
      if proc_name == 'PP8_Drell_Yan' or 'PH7' in proc_name:
          print(f"skipping {proc_name} for scale uncertainty calculation as multiweight not found in {proc_name}.yoda")
          continue
      truth_hist   = f[1] 
      #dir/file checking and create if doesn't exsits
      if not os.path.isfile(f'theory_uncertainties/root/{proc_name}_scale.root'):
        if not os.path.isfile(f'theory_uncertainties/{proc_name}.yoda'):
           print(f'theory_uncertainties/{proc_name}.yoda does not exists! Please move the rivet yoda file into theory_uncertainties and retry')
           dta_utils.printWarning(f'theory_uncertainties/{proc_name}.yoda does not exists, skipping ...')
           continue
        process_file = yoda.read(f'theory_uncertainties/{proc_name}.yoda')
        print(proc_name)
        binw  = np.array([ b.xWidth() for b in process_file[hist_name].bins() ])
        noms  = np.array([ b.sumW()   for b in process_file[hist_name].bins() ]) * unit_scale
        stat2 = np.array([ b.sumW2()   for b in process_file[hist_name].bins() ]) * (unit_scale ** 2)
        overflow = process_file[hist_name].overflow().sumW() * unit_scale
        overflow_stat2 = process_file[hist_name].overflow().sumW2() * (unit_scale ** 2)
        noms  = np.append(noms, overflow)
        stat2 = np.append(stat2, overflow_stat2)
        binw  = np.append(binw, 1.0)
        #scale_list = dta_labels.SCALES_DY if 'Sherpa' in proc_name else dta_labels.SCALES_TTBAR#or if 'DY' in proc_name or 'Drell_Yan' in proc_name else dta_labels.SCALES_TTBAR
        if 'Sherpa' in proc_name:
            scale_list = dta_labels.SCALES_DY
        elif 'Top' in proc_name:
            scale_list = dta_labels.SCALES_TOP
        else:
            scale_list = dta_labels.SCALES_TTBAR
        scaleMEPSup = noms
        scaleMEPSdn = noms
        for scale in scale_list:
            MEPS_var = np.array([ b.sumW() for b in process_file[f"{hist_name}[{scale}]"].bins()]) * unit_scale
            overflow_var = process_file[f"{hist_name}[{scale}]"].overflow().sumW() * unit_scale
            MEPS_var = np.append(MEPS_var, overflow_var)
            scaleMEPSup = list(map(max, zip(scaleMEPSup, MEPS_var)))  
            scaleMEPSdn = list(map(min, zip(scaleMEPSdn, MEPS_var)))  
      #  scaleMEPSup, scaleMEPSdn = find_scales(scale_list, process_file, hist_name, noms, noms)
        
        #find the 7-point scale uncertainties + stat uncertainty in generators
        systMEPSup = np.sqrt( stat2 + (scaleMEPSup -  noms)**2)/binw
        systMEPSdn = np.sqrt( stat2 + (scaleMEPSdn -  noms)**2)/binw

        #res_dict[proc_name] = (systMEPSup, systMEPSdn)
        #create AsymmError graph and write results to a root file
        theory_file = rt.TFile(f'theory_uncertainties/root/{proc_name}_scale.root', "RECREATE") 
        x = [truth_hist.GetBinCenter(i) for i in range(1, truth_hist.GetNbinsX() + 1)]
        y = [truth_hist.GetBinContent(i) for i in range(1, truth_hist.GetNbinsX() + 1)]
        err_xl = [truth_hist.GetBinWidth(i)/2 for i in range(1, truth_hist.GetNbinsX() + 1)]
        err_xh = err_xl
        err_yl = systMEPSdn
        err_yh = systMEPSup
        for i in range(len(x)):
           print(x[i], err_yl[i])
        print(err_yl)
        assert(len(x) == len(err_yl))
        theory_hist = rt.TGraphAsymmErrors(len(x), array('d', x), array('d', y), array('d', err_xl), array('d', err_xh), array('d', err_yl), array('d', err_yh))
        theory_hist.Write(f"{proc_name}_scale")
        print(f"scale uncertainty for {proc_name} written to file theory_uncertainties/root/{proc_name}_scale.root")
      #read the root file directly if exists alreaady
      else:
        theory_file = rt.TFile(f"theory_uncertainties/root/{proc_name}_scale.root", "READ")
        theory_hist = theory_file.Get(f"{proc_name}_scale")
        print(f"loading scale uncertainty for {proc_name} from theory_uncertainties/root/{proc_name}_scale.root")
        systMEPSdn = [theory_hist.GetErrorYlow(i) for i in range(0, truth_hist.GetNbinsX() )]
        systMEPSup = [theory_hist.GetErrorYhigh(i) for i in range(0, truth_hist.GetNbinsX() )]
      res_dict[proc_name] = (systMEPSup, systMEPSdn)
      theory_file.Close()
    return res_dict


def extract_hist_to_array(hist):
    pdf_up, pdf_dn = [], []
    for b in hist.bins:
        pdf_up.append(b.err_plus)
        pdf_dn.append(b.err_minus)
    pdf_up = np.array(pdf_up)
    pdf_dn = np.array(pdf_dn)
    return pdf_up, pdf_dn


def pdf_uncertainty(yoda_dict):
    '''find pdf uncertainty for the processes, return a dictionary with up and down errors in tuple'''
    #hist_name = "/ATLAS_STDM_2021_10:TAU_MODE=VISIBLE/mll"
    hist_name = "/ATLAS_STDM_2021_10/mll"
    res_dict = {}
    try: 
        import rivet
    except:
        print('detected rivet not setup, setting up rivet ...')
        os.system('source setupRivet.sh')
    for proc_name, f in yoda_dict.items():
       if proc_name == 'PP8_Drell_Yan' or 'PH7' in proc_name:
          print(f"skipping {proc_name} for PDF uncertainty calculation as multiweight not found in {proc_name}.yoda")
          continue
       #iterate through all input yoda files
       truth_hist   = f[1]
       #again check if the files exists already, skip if exists, create and go into the loop if not
       if not os.path.isfile(f'theory_uncertainties/root/{proc_name}_PDF.root'):
       #  if not os.path.isfile(f'theory_uncertainties/rebin_{proc_name}'):
       #     process_file = yoda.read(f[0])
       #     rebin_yoda_file(process_file, f'{proc_name}')
         process_file = yoda.read(f[0])
        #  if not os.path.isdir('theory_uncertainties/yoda_PDF'): os.mkdir('theory_uncertainties/yoda_PDF')
        #  output_yoda = f'theory_uncertainties/yoda_PDF/{proc_name}_PDF.yoda'
         #extract the corresponding pdf lists and find the PDF up and dn errors into arrays
         if 'Sherpa' in proc_name or 'sherpa' in proc_name: #or 'only' in proc_name:
         #   pdf_range = 'LHAPDF:303200-303299 '
             pdf_list = extract_pdf_list_range(process_file, 303200, 303300)    #'MUR1_MUF1_PDF_303', ' ')
             PDF_err_up, PDF_err_dn = calculate_pdf_uncertainty_manuel(process_file, 303200, 303300, pdf_list)
         elif 'vjj' in proc_name or 'VJJ' in proc_name: 
             pdf_list = extract_pdf_list_range(process_file, 303200, 303300) #'MUR1_MUF1_PDF_303', ' ')   #'_PDF_3032([0-9]{2}|300)*', '  ')
             PDF_err_up, PDF_err_dn = calculate_pdf_uncertainty_manuel(process_file, 303200, 303300, pdf_list)
        #    pdf_range = 'LHAPDF:303200-303299 '
         elif 'PP8' in proc_name:
             pdf_list = extract_pdf_list_range(process_file, 260000, 260100, pdf_pattern=r"PDF_26*") #'MUR1_MUF1_PDF_260', ' ') #'_PDF_2600([0-9]{2}|100)*', '  ')
             PDF_err_up, PDF_err_dn = calculate_pdf_uncertainty_manuel(process_file, 260000, 260100, pdf_list)
         #   pdf_range = 'LHAPDF:260000-260100 '
         else: 
            raise ValueError("unknown process name")
       #  cmd = f'rivet-calc -p {pdf_range} {f[0]} -o {output_yoda}'
       #  print(cmd)
       #  os.system(cmd)
       #  pdf_uncertainties = yoda.readYODA('theory_uncertainties/yoda_PDF/' + proc_name + '_PDF.yoda') 
         res_dict[proc_name] = (PDF_err_up, PDF_err_dn)
         print("PDF",proc_name)
         theory_file = rt.TFile(f'theory_uncertainties/root/{proc_name}_PDF.root', "RECREATE") 
         x = [truth_hist.GetBinCenter(i) for i in range(1, truth_hist.GetNbinsX() + 1)]
         y = [truth_hist.GetBinContent(i) for i in range(1, truth_hist.GetNbinsX() + 1)]
         err_xl = [truth_hist.GetBinWidth(i)/2 for i in range(1, truth_hist.GetNbinsX() + 1)]
         err_xh = err_xl
         err_yl = PDF_err_dn
         err_yh = PDF_err_up
         assert(len(x) == len(err_yl))
         theory_hist = rt.TGraphAsymmErrors(len(x), array('d', x), array('d', y), array('d', err_xl), array('d', err_xh), array('d', err_yl), array('d', err_yh))
         theory_hist.Write(f"{proc_name}_PDF")
         print(f"PDF uncertainty for {proc_name} written to file theory_uncertainties/root/{proc_name}_PDF.root")
       #read the files directly if PDF previously calculated already
       else:
         theory_file = rt.TFile(f"theory_uncertainties/root/{proc_name}_PDF.root", "READ")
         theory_hist = theory_file.Get(f"{proc_name}_PDF")
         print(f"loading PDF uncertainties for {proc_name} from theory_uncertainties/root/{proc_name}_PDF.root")
         PDF_err_dn = [theory_hist.GetErrorYlow(i) for i in range(0, truth_hist.GetNbinsX() + 1)]
         PDF_err_up = [theory_hist.GetErrorYhigh(i) for i in range(0, truth_hist.GetNbinsX() + 1)]
         res_dict[proc_name] = (PDF_err_up, PDF_err_dn)
       theory_file.Close()
    return res_dict



def combine_theory_errors(scale_dict, pdf_dict, types=['Sherpa', 'PP8', 'Sherpa_Drell_Yan', 'nominal']):# 'Drell_Yan_only']):
    '''combining the up and down err band for each process'''
    res = {}
    for err_type in types:
        if err_type == 'Sherpa_Drell_Yan':
            scale_array = np.array([ [up, dn] for key, (up, dn) in scale_dict.items() if (err_type in key)])
            if pdf_dict: 
               pdf_array = np.array([ [up, dn] for key, (up, dn) in pdf_dict.items() if (err_type in key) ])
        elif err_type != 'nominal':
            scale_array = np.array([ [up, dn] for key, (up, dn) in scale_dict.items() if (err_type in key or 'SingleTop' in key or 'WT' in key or 'VJJ' in key)]) 
            if pdf_dict:
               pdf_array = np.array([ [up, dn] for key, (up, dn) in pdf_dict.items() if (err_type in key or 'SingleTop' in key or 'WT' in key or 'VJJ' in key) ])
        elif err_type == 'nominal': 
            scale_array = np.array([ [up, dn] for key, (up, dn) in scale_dict.items() if (key == 'Sherpa_Drell_Yan' or key == 'PP8_TTBAR' or 'SingleTop' in key or 'WT' in key or 'VJJ' in key)])
            if pdf_dict:
               pdf_array = np.array([ [up, dn] for key, (up, dn) in pdf_dict.items() if (key == 'Sherpa_Drell_Yan' or key == 'PP8_TTBAR' or 'SingleTop' in key or 'WT' in key or 'VJJ' in key)])
        tot_scale_up2, tot_scale_dn2 = np.sum(scale_array**2, axis=0)
        tot_pdf_up2, tot_pdf_dn2 = np.zeros(len(scale_array)), np.zeros(len(scale_array))
       
        if pdf_dict: 
            tot_pdf_up2, tot_pdf_dn2 = np.sum(pdf_array**2, axis=0)
        TOTAL_UP_ERR = np.sqrt(tot_scale_up2 + tot_pdf_up2) if pdf_dict else np.sqrt(tot_scale_up2)
        TOTAL_DN_ERR = np.sqrt(tot_scale_dn2 + tot_pdf_dn2) if pdf_dict else np.sqrt(tot_scale_dn2)
        res[err_type] = (TOTAL_UP_ERR, TOTAL_DN_ERR)
    for t, err in res.items():
        print(t, err)
    return res
       


def extract_pdf_list(aos, pdf_pattern, pdf_unpattern):
    h_list = [h.path() for h in aos.values() if pdf_pattern in h.path()]
    pdf_list =list(set([i.split('[')[-1][:-1] for i in h_list if pdf_unpattern not in i]))
    return pdf_list

def extract_pdf_list_range(aos, lower_range, upper_range, pdf_pattern="MUR1_MUF1_"): #_PDF_2929292_
    '''extract the required PDF list from the yoda file into a list of pdf names'''
    pattern = re.compile(pdf_pattern)
    h_list = [h.path() for h in aos.values() if pattern.search(h.path())]
    pdf_list = [ ]
    for h in h_list:
       if not 'mll[' in h: continue
       if 'RAW' in h: continue
       if 'ME_ONLY' in h: continue
       if 'ASSEW' in h: continue
       if 'EXPASS' in h: continue
       if 'MULTI' in h: continue
       if pdf_pattern == "MUR1_MUF1_": 
          pdf_name = h.split('[')[-1].split('_')
          PDF = [n for n in pdf_name if 'PDF' in n][0]
          PDF = PDF.lstrip('PDF').rstrip(']')
       else:
          PDF = h.split('[')[-1].split('_')[2]
       if lower_range <= int(PDF) <= upper_range:
          pdf_list.append(h.split('[')[-1][:-1])
#    pdf_list = [h for h in h_list if lower_range <= int(h.split('[')[-1].split('_')[2].lstrip('PDF')) <= upper_range]
    return pdf_list


def reassign_labels(combined_dict):
    updated_key = {}
    for key, val in combined_dict.items():
        if key == 'Sherpa': updated_key[key] = 'SM (Sherpa 2.2)'  #'Sherpa DY + Sherpa ttbar' #SM (Sherpa DY+ttbar)'
        elif key == 'PP8' : updated_key[key] = 'SM (PP8)' #'PP8 DY + PP8 ttbar' #'SM (PP8 DY+ttbar)'
        elif key == 'Sherpa_Drell_Yan': updated_key[key] = 'Sherpa DY only'
        elif key == 'nominal': updated_key[key] = 'Nominal MC Stack' #or delete MC nominal as not really needed
    for old_key, new_key in updated_key.items():
        combined_dict[new_key] = combined_dict.pop(old_key)
    return  

def rebin_histo(hist, new_bin_edges, path):
    rebinned_hist = yoda.Histo1D(new_bin_edges)
    overflow  = 0
    underflow = 0
    for b in hist.bins():
       x = b.xMid()
       y = b.sumW()
       y_err = b.sumW2()
       for i in range(len(new_bin_edges) - 1):
          if x < new_bin_edges[0]: underflow += y
          elif x >= new_bin_edges[-1]: overflow += y
          else: rebinned_hist.fill(x, y, y_err)
        #  if new_bin_edges[i] <= x < new_bin_edges[i + 1]:
        #      rebinned_hist.fill((new_bin_edges[i] + new_bin_edges[i + 1]) / 2, y)
        #      break
   # rebinned_hist.fill(new_bin_edges[0], underflow)
    rebinned_hist.fill(new_bin_edges[-1], overflow) 
    rebinned_hist.setPath(path)
   #    rebinned_hist.fill(x, y)
    return rebinned_hist


def rebin_yoda_file(yoda_hist, filename, new_bin_edges=dta_labels.BINNING['mll']):
    rebin_histos = {}
    for path, hist in yoda_hist.items():
       #  if type(hist) == 'yoda.core.Histo1D' and path.split('/')[-1].startswith('mll['):
        if isinstance(hist, yoda.Histo1D) and 'mll' in path: #path.split('/')[-1].startswith('mll[') or path == '/ATLAS_STDM_2021_10:TAU_MODE=VISIBLE/mll':
           rebin_histos[path] = rebin_histo(hist, new_bin_edges, path)
           print(f"rebinning {path}")
  #      else:
  #         rebin_histos[path] = hist
    yoda.write(rebin_histos, 'theory_uncertainties/rebin/' + filename)




def merge_binss(hist, start=-2, end=-1):
    bins = hist.bins()
    if len(bins) < 2: return hist
    merged_sumW  = 0
    merged_sumW2 = 0
#    for i in range(end-1, start-1, -1):
#        merged_sumW  += hist[i].sumW()
#        merged_sumW2 += hist[i].sumW2()
    last_bin = bins[-1]
    second_last_bin = bins[-2]
    merged_sumW = last_bin.sumW() + second_last_bin.sumW()
    merged_sumW2 = last_bin.sumW2() + second_last_bin.sumW2()

   # hist[start].setSumW(merged_sumW)
   # hist[start].setSumW2(merged_sumW2)
    new_hist = yoda.Histo1D(dta_labels.BINNING['mll'])
    for i in bins[:start]:
        bin_width = i.xMax() - i.xMin()
        new_hist.fill(i.xMid(), i.sumW()/bin_width, i.sumW2()/(bin_width**2))
    #new_hist.addBin(yoda.HistoBin1D(new_min, new_max, merged_sumW, merged_sumW2))
    merged_bin = yoda.HistoBin1D(new_min, new_max)
    merged_bin.fill(new_sumW, new_sumW2)
    new_hist.addBin(merged_bin)
    new_mid = (second_last_bin.xMid() + last_bin.xMid()) / 2.0
    new_hist.fill(new_mid, merged_sumW, merged_sumW2)
  #  for i in range(end, start, -1):
  #      hist.rmBin[i]
  #  hist.replaceBin(end, new_bin)
    return new_hist
        

def merge_bins(hist):
    bins = hist.bins()
    
    if len(bins) < 2:
        return hist

    x_min = hist.xMin()
    x_max = bins[-1].xMax()  # Upper edge of the last bin
    new_hist = yoda.Histo1D(dta_labels.BINNING['mll'])

    for b in bins[:-2]:
        new_hist.fill(b.xMid(), b.sumW(), b.sumW2())
    last_bin = bins[-1]
    second_last_bin = bins[-2]
    
    new_min = second_last_bin.xMin()
    new_max = last_bin.xMax()
    new_sumW = second_last_bin.sumW() + last_bin.sumW()
    new_sumW2 = second_last_bin.sumW2() + last_bin.sumW2()

    new_hist.fill((new_min + new_max) / 2, new_sumW, new_sumW2)

    return new_hist
