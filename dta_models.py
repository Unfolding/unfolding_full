
import ROOT as rt
rt.SetMemoryPolicy(rt.kMemoryStrict)
rt.gROOT.SetBatch(True)
rt.TH1F.SetDefaultSumw2(True)
rt.gErrorIgnoreLevel = rt.kWarning

import collections
import dta_utils, dta_labels
import numpy as np
import glob
import sys
from array import array
import dta_labels
CP2SKIP = [ ]

XSECS = { }

#the unfolding class
class IBUengine(object):
  def __init__(self):
    self.matrix = rt.RooUnfoldResponse(dta_utils.randstr(5), "")
    self.algo = rt.RooUnfoldBayes(dta_utils.randstr(5), "")
    self.algo.SetSmoothing(False)
    self.algo.HandleFakes(False)
    self.cached = False

  def _reset(self):
    if not self.cached:
      self.cached = True
      return
    self.matrix.ClearCache()
    self.matrix.UseOverflow(False)
    self.algo.ForceRecalculation()
    self.algo.Hmeasured().Delete()
    self.algo.SetSmoothing(False)
    self.algo.HandleFakes(False)

  def unfold(self, data, reco, truth, transfer, k = 4):
    self._reset()
    self.matrix.Setup(reco, truth, transfer)
    self.matrix.UseOverflow(True)
    self.algo.Setup(self.matrix, data)
    self.algo.SetIterations(k)
    self.algo.SetVerbose(-1)
    return self.algo.Hunfold()

  def toysRMS2(self, data, reco, truth, transfer, k, rng, nToys = 2000):
    nBins = data.GetNbinsX() + 2
    toy_res = np.zeros((nToys, nBins))
    for i in range(nToys):
        toy_data = dta_utils.fluctPoisson(data, rng)
        res = self.unfold(toy_data, reco, truth, transfer, k)
        toy_res[i,:] = [ res.GetBinContent(j) for j in range(nBins) ]
        toy_data.Delete()
        res.Delete()
    return ((toy_res - toy_res.mean(axis=0)) ** 2).mean(axis=0)


#the sampling class
class Sample(object):

  def __init__(self, model, input_dir, debug = False):

      self.process = str(model)
      self.files = { }
      self.files_names = { }
      self.model = model
      fpaths = [ ] # all files associated with regex
      for regex in dta_labels.REGEX_MAP[model].split('|'):
          fpaths.extend( glob.glob(input_dir + regex) )
      if not len(fpaths):
        sys.exit('[FATAL] - No files found for %s.' % model)
      #print (self.process, fpaths)
      self.needsLumi = 'RxslM' not in fpaths[0]
      #check if CP name in file names and read them into different keys
      #update: since we do not need to run all systs now, this is not nessessary,
      # but function is kept in case future changes.
      for fpath in fpaths:
         thisModel = model
         if "BIAS" in fpath:
            thisModel += "_BIASVAR"
         elif "DATA" in fpath:
            thisModel += "_DATA"
         elif "FAKES" or "fakes" in fpath:
            thisModel += "_FAKES"
         elif "BSM" in fpath:
            thisModel += "_BSM"
         else:
            thisModel += "_ALL"
       #  elif "ALL" in fpath: #this is used if jobs are not splitted by systematic groups.
       #     thisModel += "_ALL"
         if not thisModel in self.files:
            self.files[thisModel] = []
            self.files_names[thisModel] = []
         self.files[thisModel].append(rt.TFile.Open(fpath, 'READ'))
         self.files_names[thisModel].append(fpath)
      self.scalings = { }
      for model, fpaths in self.files_names.items():
          for i, fpath in enumerate(fpaths):
              #getting the process name (e.g. sh_2211_Ztautau ....) + the tag name (e.g. e....)
              if "BIASVAR" or "DATA" in fpath:
                 tag = ''.join(fpath.split('/')[-1].split('.')[2:4])   #[2:4] if not using Simon
              elif "FAKES" or "fakes" in fpath:
                 tag = "FAKES"
              else:
                 tag = ''.join(fpath.split('/')[-1].split('.')[3:5])   #[2:4] if not using Simon
              #if not pre rescaled, scale it by the corresponding xsec, sumofweight etc. if already rescaled (include fakes), scale it by 0.001 for convertion
              sf = 1.0  #scaling from 1/fb to 1/pb because postprocessing use 138.9fb-1 as LUMI
              ctg = fpath.split('/')[-1]
              if not any([ x not in ctg for x in ['Rxs', 'DATA', 'FAKES', 'fakes', 'BSM'] ]):
              #new fixes trying to accomodate the new changes for the new set of productions
                  sf = XSECS[tag] / self.files[model][i].Get('sumOfWeights').GetBinContent(4)
              if 'MC16' in ctg.split('.')[5]:
                  sf *= dta_labels.lumi[ctg.split('.')[5]]
              elif not 'DATA' == ctg.split('.')[1] or 'FAKES' == ctg.split('.')[1]:
                  sf *= dta_labels.lumi['fullLumi']
              if not model in self.scalings:
                  self.scalings[model] = [ ]
              self.scalings[model].append(sf)
      if debug:
          dta_utils.printDebug('Scale factors used for the input files from ' + model + ' are:')
          for proc, sf in self.scalings.items():
              print('\t'+proc, sf)


  #find the set of files with the largest stats as nominal, return the model+CP str
  def get_nominal(self):
      """find the max effNumEntry set"""
      effNumEntry = []
      max_stat = 0
      if len(self.files) == 1: return list(self.files.keys())[0]
      for model, fpaths in self.files.items():
          #if "_BIASVAR" in model: continue #i.e. the current BIASVAR truth is having problems so cannnt be taking into account
          if "DATA" or "FAKE" in model: continue
          sumw = fpaths[0].Get('sumOfWeights').GetBinContent(4)
          sumw2 = fpaths[0].Get('sumOfWeights').GetBinContent(5)
          for fpath in fpaths[1:]:
              sumw += fpath.Get('sumOfWeights').GetBinContent(4)
              sumw2 += fpath.Get('sumOfWeights').GetBinContent(5)
          eff = sumw**2 / sumw2
          if eff > max_stat:
             effNumEntry.append(model)
             max_stat = eff
      return effNumEntry.pop()

  #only use the max_stat model set for stacking nominal, if syst != empty then go to the corresponding set of input files
  def get_histo(self, hname, rebin):
      """lowest-level histogram stacking function, this is to stack each process individually"""
      model_in = None
      if "bias_var" in hname:
         for model_set in self.files.keys():
            if "BIASVAR" in model_set:
                model_in = model_set
                break
      #now make the class instance for different sets are different, could modify later
      else:
         #if not bias in hname, then it is either data or ALL or fakes (call instances separately)
         for model_set in self.files.keys():
             model_in = model_set
         hname = hname.rstrip('/')
      #print(self.files_names[model_in][0])
      #print(self.files[model_in][0], model_in)
      raw = self.files[model_in][0].Get(hname)
      try:
          rtn = raw.Clone(dta_utils.randstr(5))
      except:
          raise Exception(f"the histogram {hname} is not found in model {model_in}")
      raw.Delete()
      rtn.Scale(self.scalings[model_in][0])
      #normal loop if it happens to be within the real nominal set
      for i in range(1, len(self.files[model_in])):
          #print(self.files[model_in][i])
          tmp = self.files[model_in][i].Get(hname)
          tmp.Scale(self.scalings[model_in][i])
          #print(self.scalings[model_in][i], self.files[model_in][i])
          rtn.Add(tmp)
          tmp.Delete()
      if rebin and 'res' in hname:
          binning = array('d', dta_labels.BINNING[rebin])
          rtn = dta_utils.rebin2D(rtn, binning, binning)
      elif rebin:
          binning = array('d', dta_labels.BINNING[rebin])
          rtn = rtn.Rebin(len(binning)-1, dta_utils.randstr(5) + '_rebinned', binning)
      return rtn


  def get_fake_systs(self): #group):
      rtn = collections.defaultdict(list)
      if not 'FAKE' in self.process:
         raise Exception("Fakes are not run: cannot get fake systematics. Please check the process input for the class instance")
      hn = "mll_HighMassOSs2thh"
      for key in self.files["FAKES_FAKES"][0].Get('rec_' + hn).GetListOfKeys():
         name = key.GetName()
         if 'nominal' in name: continue
         #if not group in name: continue
         if 'mc_' in name or 'data_' in name: continue   #filter out the mc and data systs to only use the nominal in the syst list
         syst_name = name #.replace('__', '_')
         #check the systs and group the up dn ones
         rtn['FAKES_ERR_TOTAL'].append(syst_name)
      return rtn


  def get_BSM_systs(self, syst_name):
      rtn = collections.defaultdict(list)
      if not 'BSM' in self.process:
         raise Exception("BSM is not run, cannot proceed to get the BSM systematic uncertainties")
      hn = 'mll_HighMassOSs2thh'
      for key in self.files['BSM_BSM'][0].Get('rec_' + hn).GetListOfKeys():
          name = key.GetName()
          if syst_name and name == syst_name:
             return rtn['BSM_ERR_TOTAL'].append(syst_name) #may need changes when confirmed
          if 'nominal' in name: continue
          if 'mc_' or 'data_' in name: continue
      return rtn



  def get_BSM_weights(self):  #need to confirm the weight usage for the BSM inputs
      rtn = collections.defaultdict(list)
      if not 'BSM' in self.process:
         raise Exception("BSM is not run, cannot proceed to get all BSM weights")
      for key in self.files["BSM_BSM"][0].Get('tru_' + hn).GetListOfKeys():
         name = key.GetName()
         if 'nominal' in name: continue
         syst_name = name
         rtn[name].append(syst_name)
      return rtn

  def get_systs(self, debug = False):
      """get the avaiable systematic uncertaitnies"""
      rtn = { }
      reg = 'HighMassOSs2thh' # assumes this always exist
      obs = "mll_vs_Nbjet"    # also assume this observable always exist
      hn = obs + '_' + reg
      if debug:
          print("groups of systs considered:")
          print(list(self.files.keys()))
      for model in self.files.keys():
        # don't take the signal-injection names into account
        if "BIASVAR" in model:
          continue
        # check for keys in all first file in all syst categories to get the full syst list
        for key in (self.files[model][0].Get('rec_' + hn).GetListOfKeys()):
          name = key.GetName()
          if 'nominal' in name:   continue
         # if 'rec_' not in name:  continue
          if '__' not in name:    continue
         # if reg not in name:     continue
         # if obs not in name:     continue
         #  systname = name[name.find(reg) + len(reg) :]
          systname = name
          if any([ systname.startswith(cp) for cp in CP2SKIP ]):
             continue
          new_syst_name = systname.replace('__', '_')
          # clear ambiguities in the syst category names
          if "TRUEH" in new_syst_name:
             syst_name = "_".join(new_syst_name.split('_')[0:4])
          else:
             syst_name = "_".join(new_syst_name.split('_')[0:3])
          #print("syst=  " + syst_name)
          # split all syst into corresponding categories
          if "PRW" in systname:
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
          elif "TAU" in systname:
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
          elif "EL" in systname:
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
          elif "EG" in systname:
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
          elif "FT" in systname:
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
          elif "MUON" in systname:
              if "MUON" not in rtn:
                 rtn["MUON"] = []
              if not "EFF" in syst_name:
                 rtn["MUON"].append(systname)
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
          elif "JET" in systname:
              if syst_name not in rtn:
                 rtn[syst_name] = [ ]
              rtn[syst_name].append(systname)
      return rtn

  def finish(self):
      for key in self.files:
          for f in self.files[key]:
              f.Close()


#histogram stacking class (called in Sample class too)
class Stack(object):

    def __init__(self, processes, input_dir, isVisible = True,
                       withSmoothing = False, debug = False): #,isData = None):
        self.vis_prefix = 'vis_' if isVisible else ''
        self.nom_models = [ ]
        self.alt_models = [ ]
        self.samples = { }
        for proc in processes:
            for model in dta_labels.PROCESS_MAP[proc]:
                if dta_labels.PROCESS_MAP[proc].index(model):
                    self.alt_models.append(model)
                else:
                    self.nom_models.append(model)
                self.samples[model] = Sample(model, input_dir, debug)
        self.config = [ model for model in self.nom_models ]
        self.syst_list = self.samples[self.config[0]].get_systs(debug)
        if 'FAKES' in processes:
           self.fake_syst = self.samples[self.config[0]].get_fake_systs()
        else:
           self.fake_syst = { }
        self.axisConfig = rt.TFile('data/config_2Daxes.root', 'READ')
        self.axes = None
        self.useSmoothing = withSmoothing
        self.hname = ''
        self.syst = 'nominal'
        self.rebin = None

    def load(self, hname, model = 'None', syst = '', rebin = '', debug = False):
        """loading the class parameters into the instance and get used by the hitogram calling methods"""
        if hname not in self.hname:
            if self.axes != None:
                xaxes, yaxis = self.axes
                yaxis.Delete()
                for xaxis in xaxes:
                    xaxis.Delete()
            self.axes = self.get_axes(hname)
        if self.hname != hname:
            self.hname = hname
        if self.syst != syst:
            self.syst = syst
        self.config = [ m if model.split('_')[0] not in m else model for m in self.nom_models ]
        if rebin in dta_labels.BINNING:
            self.rebin = rebin
        #if debug:
            #print('loaded:', self.hname, self.syst, model, self.config)
        return hname

    def get_axes(self, hname):
      if '_vs_' in hname:
        yaxis = self.axisConfig.Get('yax_%s' % hname)
        xaxes = [ ]
        for i in range(yaxis.GetNbins() + 2):
            xaxes.append( self.axisConfig.Get('xax%i_%s' % (i, hname)) )
        assert( len(xaxes) == yaxis.GetNbins() + 2)
        return (xaxes, yaxis)
      return None


    def variations(self):
      return list(self.alt_models)

    def fake_systs(self):
      return dict(self.fake_syst)

    def systematics(self):
      return dict(self.syst_list)

    def get_stack(self, hname, syst, rescale = None):
        """Helper method to assemble a stack of samples."""

        hpath = hname + '/' + syst
        rtn = self.samples[self.config[0]].get_histo(hpath, self.rebin)
        if rescale and rescale[0] in self.config[0]:
            rtn.Scale(rescale[1])

        for i in range(1, len(self.config)):
            tmp = self.samples[self.config[i]].get_histo(hpath, self.rebin)
            if rescale and rescale[0] in self.config[i]:
                tmp.Scale(rescale[1])
            rtn.Add(tmp)
            tmp.Delete()
        if self.rebin:
            binning = array('d', dta_labels.BINNING[self.rebin] + [700.0])
            rtn = dta_utils.show_overflow(rtn, '13 TeV', binning, binning) if 'res' in hname else \
                  dta_utils.show_overflow(rtn, '13 TeV', binning)
        return rtn


    def get_reco(self, rescale = None):
        """get reco-level histogram"""
        return self.get_stack('rec_' + self.hname, self.syst, rescale)


    def get_truth(self, var = False, rescale = None):
        """get MC_truth histogram"""
        return self.get_stack(self.vis_prefix + 'tru_' + self.hname,
                              self.syst if var else  'nominal', rescale)

    def get_tbo(self):
        """get truth-level value of the events passing both selections"""
        return self.get_stack(self.vis_prefix + 'tbo_' + self.hname, self.syst)

    def get_rbo(self):
        """get reco-level value of the events passing both selections"""
        return self.get_stack(self.vis_prefix + 'rbo_' + self.hname, self.syst)

    def apply_fid_corr_to(self, histo):
        """apply the fiducial correction factor (events at reco-level that
           do not have a truth equivalents, i.e. MC fakes) """
        corr = self.get_stack(self.vis_prefix + 'rbo_' + self.hname, self.syst)
        rec = self.get_reco()
        corr.Divide(rec)
        errs = histo.GetSumw2()
        for i in range(histo.GetNbinsX() + 2):
            sf = corr.GetBinContent(i)
            histo.SetBinContent(i, sf * histo.GetBinContent(i))
            errs.AddAt(errs.At(i)*sf*sf, i)
        rec.Delete()
        corr.Delete()

    def get_fake_rate(self):
        rec = self.get_reco()
        rec.ClearUnderflowAndOverflow()
        rbo = self.get_stack(self.vis_prefix + 'rbo_' + self.hname, self.syst)
        rbo.ClearUnderflowAndOverflow()
        for i in range(rec.GetNbinsX()+2):
          if rec.GetBinContent(i) < 0.:
            rec.SetBinContent(i, 0.0)
          if rbo.GetBinContent(i) < 0.:
            rbo.SetBinContent(i, 0.0)
        residual = rec.Clone()
        residual.Add(rec, rbo, 1.0, -1.0)
        for i in range(rec.GetNbinsX()+2):
          if residual.GetBinContent(i) < 0.:
            residual.SetBinContent(i, 0.0)
        rtn = rt.TGraphAsymmErrors(residual, rec, 'b(1,1) sh mode e0')
        residual.Delete()
        rbo.Delete()
        rec.Delete()
        return rtn

    def get_fid_efficiency(self, truth=None, tbo_=None):
        tru = self.get_truth() if not truth else truth
        tru.ClearUnderflowAndOverflow()
        if not tbo_:
           tbo = self.get_stack(self.vis_prefix + 'tbo_' + self.hname, self.syst)
        else: tbo = tbo_
        tbo.ClearUnderflowAndOverflow()
        rtn = rt.TGraphAsymmErrors(tbo, tru, 'b(1,1) sh mode e0')
        tbo.Delete()
        tru.Delete()
        return rtn

    def get_response(self):
        """get response matrix"""
        rtn = self.get_stack(self.vis_prefix + 'r_res_' + self.hname, self.syst)
        if self.useSmoothing:
            rtn = dta_utils.applyWMA(rtn)
        return rtn

    def get_purity(self):
        """finds the purity (events that have the same reco-level and truth-level bins) """
        rbo = self.get_rbo()
        resp  = self.get_response()
        diag  = rbo.Clone(rbo.GetName() + '_purity')
        err1D = diag.GetSumw2()
        err2D = resp.GetSumw2()
        for i in range(resp.GetNbinsX()+2):
            b = resp.GetBin(i, i)
            v = resp.GetBinContent(b)
            t = rbo.GetBinContent(i)
            diag.SetBinContent(i, t if v > t else v)
            err1D.AddAt(err2D.At(b), i)
        rtn = rt.TGraphAsymmErrors(diag, rbo, 'b(1,1) sh mode e0')
        resp.Delete()
        diag.Delete()
        rbo.Delete()
        return rtn

    def finish(self):
        self.axisConfig.Close()
        for key in self.samples:
           self.samples[key].finish()

