
# Setting up the process maps
# - specify the different sets of input files to the alias
PROCESS_MAP = {
  # map of processes to be considered
  # the first array entry is the nominal
  # additional elements are modelling variations
  'DATA'    : [ 'DATA' ],                                   # observed data
  'ZTT'     : [ 'ZTT_SH', 'ZTT_PP8' ],                     # fiducial signal
  'HMZTT'   : [ 'HMZTT_SH'],
  'ZTT_SH'  : [ 'ZTT_SH' ],
  'ZTT_PP8' : [ 'ZTT_PP8'],
  'TTBAR'   : [ 'TTBAR_PP8', 'TTBAR_PP8_pT', 'TTBAR_PP8_SYST', 'TTBAR_PH7' ],#'TTBAR_SH', 'TTBAR_PP8_pT',],#, 'TTBAR_PP8', 'TTBAR_PH7'], # fiducial signal
  'TTBAR_SH': [ 'TTBAR_SH' ],
  'TTBAR_PP8' : [ 'TTBAR_PP8' ],
  'VJJ_SH'  : ['VJJ_SH'],
  'WTOP_DR' : ['WTOP_DR'],
  'SINGLE_TOP_PP8' : ['SINGLE_TOP_PP8'],
 # 'TTBAR'   : [ 'TTBAR_PP8', 'TTBAR_PH7','TTBAR_MG5P8' ],                             # fiducial signal
  'WT'      : [ 'WTOP_DR', 'WTOP_DS' ],                     # fiducial signal
  'LLJJ'    : [ 'LLJJ_SH', ],                               # fiducial signal
  'VVV'     : [ 'VVV_SH', ],                                # fiducial signal
  'VJJ'     : [ 'VJJ_SH' ],
  'HIGGS'   : [ 'HIGGS_PP8',],
  'WLV'     : [ 'WLV_SH', ],                                # non-fiducial background
  'ZLL'     : [ 'ZLL_SH', ],                                # non-fiducial background
  'VV'      : [ 'VV_SH', ],                                 # non-fiducial background
  'TOP'     : [ 'SINGLE_TOP_PP8', 'SINGLE_TOP_H7' ],                               # non-fiducial background
  'BSM_INT' : [ 'BSM_INT_MG5P8', ],                         # signal injection
  'BSM_RES' : [ 'BSM_RES_MG5P8', ],                         # signal injection
  'BSM_INTSLQ' : [ 'BSM_INT_SLQ' ],
  'BSM_RESSLQ' : [ 'BSM_RES_SLQ' ],
  'FAKES'   : [ 'FAKES' ],                                  # fake estimation files
  'BIAS'    : [ 'BIAS' ],
}

#AVOID_REGEX = ['BSM'
REGEX_MAP = {
  'DATA'          : r'*data*.period*',
  'ZTT_SH'        : r'user.*.*Sh_2211_Ztautau_*e8373*.MR*|user.*.*Sh_2211_Ztautau_mZ*.MR*',
  'ZTT_PP8'       : r'*Powheg*tautau*',
  'TTBAR_SH'      : r'user.*.*Sh_2212_ttbar*.MR*',
  'HMZTT_SH'      : r'user.*.*120_ECMS_BFilter*',         #CVetoBVeto*',
 # 'TTBAR_PP8'     : r'*PP8_ttbar*',                        #r'*PP8_ttbar_*_nonallhad*',
  'TTBAR_PP8'     : r'*PP8_ttbar_hdamp258*',
  'TTBAR_PP8_pT'  : r'*PP8_ttbar_pThard1*',
  'TTBAR_PP8_SYST': r'*PP8_ttbar_hdamp517*',
  'TTBAR_PH7'     : r'*PowhegHerwig7*_tt_*',
  'TTBAR_MG5P8'   : r'*aMcAtNloPy8*_ttbar*',
#  'TOP_PP8'       : r'*PP8*schan*top.e*|*PP8*tchan*top.e*',
  'WTOP_DR'       : r'*PP8*Wt_DR*top.e*',
  'WTOP_DS'       : r'*PP8*Wt_DS*top.e*',
  'SINGLE_TOP_PP8': r'*PP8_singletop*|*PP8_tchan_BW50*',
  'SINGLE_TOP_H7' : r'*PhH7EG_H7UE_716_*',
  'LLJJ_SH'       : r'*Sh_2212_ll*os*|*Sh_2211_*Zll*',
  'VJJ_SH'        : r'*Ztt2jets*',
  'VV_SH'         : r'*Sh_2212_lv*|*Sh_2212_ll*ss*|*Sh_2212_lll*|*Sh_2211_Wlv*',
  'VVV_SH'        : r'*Sh222_*_EW6*', # TODO: split into llXXXX and the rest?
  'WLV_SH'        : r'*Sh_2211_W*',
  'ZLL_SH'        : r'*Sh_2211_Zee*|*Sh_2211_Zmumu*',
  'BSM_INT_MG5P8' : r'*VLQZpIntFil*', #*MGPy8_tautau_scalarLQ_interference_bFilter*
  'BSM_RES_MG5P8' : r'*VLQZpBSMFil*', #*MGPy8_tautau_scalarLQ_pureBSM_bFilter*
  'BSM_INT_SLQ'   : r'*MGPy8_tautau_scalarLQ_interference_bFilter*',
  'BSM_RES_SLQ'   : r'*MGPy8_tautau_scalarLQ_pureBSM_bFilter*',
  'FAKES'         : r'*FAKES*',#|user.*.fakes*',
  'BIAS'          : r'/BIASVAR/*BIASVAR*Ztautau*|/BIASVAR/*BIASVAR*ttbar*',#|*BIASVAR*',
  'HIGGS_PP8'     : r'*ggH125*',
}

#BSM g^2 map
g2_map = {
   'INC_1_5_g2' : {
     's2teh' : 6.0228,
     's2tmh' : 7.6751,
     's2thh' : 5.5,#4.46,#957,
   },

   '0bjet_1_5_g2' : {
     's2teh' : 45.2352,
     's2tmh' : 29.6371,
     's2thh' : 16.7992,
   },

   '1bjet_1_5_g2' : {
     's2teh' : 6.1868,
     's2tmh' : 7.7109,
     's2thh' : 8.2082,
   },

   '2bjet_1_5_g2' : {
     's2teh' : 68.9536,
     's2tmh' : 136.4259,
     's2thh' : 17.4958,
   },

   'INC_1_4_g2' : {
     's2thh' : 4.2983,
   },
}

ALLOWED_FORMAT = ['pdf', 'png', 'jpg']

#label alias used in the plots
XLABELS = {
  'mll' : 'm_{ll} [GeV]',
  'mll_vs_Nbjet' : 'm_{ll} vs N_{b} [GeV]',
  'mll_1b' : 'm_{ll}^{1b}',
  'llpT' : 'p_{T}^{ll} [GeV]',
}

YLABELS = {
  'mll' : 'dm_{ll}',
  'mll_1b' : 'dm_{ll}^{1b}',
  'mll_vs_Nbjet' : 'dm_{ll}dN_{b}',
  'llpT' : 'dp_{T}^{ll}',
}

HAS_GEV = [ 'mll', 'mll_vs_Nbjet', 'llpT', 'mll_vs_Nbjet' ]

CP_TYPES = [ 'tau', 'ftag', 'egamma', 'muon', 'jet' ] # separate out trigger? ftag egamma muon

#alias for the uncertainty plot
SYST_CONFIG = {
  ''       : ('',       'Total',       1), # kBlack
  'stats'  : ('stats',  'Stats',     802), # kOrange+2
  'model'  : ('MODEL_', 'Modelling', 862), # kAzure+2
  'tau'    : ('TAUS_',  'Tau CP',    629), # kRed-3
  'ftag'   : ('FT_',    'FTAG CP',   418), # kGreen+2
  'muon'   : ('MUON_',  'Muon CP',   434), # kCyan+2
  'egamma' : ('EL_',    'Egamma',    892), # kPink-8
  'jet'    : ('JET_',   'JET',       880), # kViolet
  'fakes'  : ('FAKES_', 'Fakes'    , 600), # kblue
  'unfolding'   : ('UNFOLDING', 'Unfolding',  397), # kGreen+2# 397 kyellow-3
}


lumi = {}
lumi["2015"] = 3.24454 #fb^{-1}
lumi["2016"] = 33.4022 #fb^{-1}
lumi["2017"] = 44.6306 #fb^{-1}
lumi["2018"] = 58.7916 #fb^{-1}

lumi["MC16a"] = lumi["2015"] + lumi["2016"]
lumi["MC16d"] = lumi["2017"]
lumi["MC16e"] = lumi["2018"]

lumi["fullLumi"] = lumi["2015"] + lumi["2016"] + lumi["2017"] + lumi["2018"]

BINNING = {
  #'mll' : [ 60., 70., 80., 90., 100., 115., 130., 145., 160., 175., 205.,
  #          235., 275., 315., 355., 395., 455., 515., 560., 635.],
  #'mll'  : [ 100., 115., 130., 145., 160., 175., 205., 235.,
  #           275., 315., 355., 395., 455., 515., 560., 635.],
  'mll'  : [ 100., 145., 160., 175., 205., 235.,
             275., 315., 355., 395., 455., 515., 635. ],
  'llpT' : [   0.,  20.,  40.,  60.,  80., 100., 120., 160.,
             200., 300., 480., ],
}


SCALES_DY = [
  'MUR05_MUF05_PDF303200_PSMUR05_PSMUF05',
  'MUR05_MUF1_PDF303200_PSMUR05_PSMUF1',
  'MUR1_MUF05_PDF303200_PSMUR1_PSMUF05',
  'MUR1_MUF2_PDF303200_PSMUR1_PSMUF2',
  'MUR2_MUF1_PDF303200_PSMUR2_PSMUF1',
  'MUR2_MUF2_PDF303200_PSMUR2_PSMUF2',
 ]
 
SCALES_TTBAR = [
  '_muR_05_muF_05_',
  '_muR_05_muF_10_',
  '_muR_10_muF_05_',
  '_muR_10_muF_20_',
  '_muR_20_muF_10_',
  '_muR_20_muF_20_'
 ]

SCALES_TOP = [
  '_muR_050_muF_050_',
  '_muR_050_muF_100_',
  '_muR_100_muF_050_',
  '_muR_100_muF_200_',
  '_muR_200_muF_100_',
  '_muR_200_muF_200_'
 ]

