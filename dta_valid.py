import ROOT as rt
rt.SetMemoryPolicy(rt.kMemoryStrict)
rt.gROOT.SetBatch(True)
rt.TH1F.SetDefaultSumw2(True)
rt.gErrorIgnoreLevel = rt.kWarning

import AtlasStyle
import dta_utils, dta_plot
AtlasStyle.setAtlasDefaults()

import numpy as np
import ctypes, glob, random, string
from array import array
import collections
from scipy import stats
from scipy import ndimage
import math

LUMI = None

def bias_significance(data, reco, truth, response, bias_res, k, rand, nToys, stat, ibu):
   """function used for an alternative method to determine the optimised no. of iterations using statistical variations"""
   new = truth.Clone(dta_utils.randstr(5))
   stats = truth.Clone(dta_utils.randstr(5))
   nBins = data.GetNbinsX() + 2
   toy_res_num = np.zeros(nToys)
   toy_res_bias = np.zeros(nBins)
   average_val = np.zeros(nBins)
   bias_sig = np.zeros((nToys, nBins))
   total_stats = np.zeros((nToys, nBins))
   total_val = np.zeros((nToys, nBins))
   for n in range(nToys):
      toy = data.Clone(dta_utils.randstr(5))
      rec = reco.Clone(dta_utils.randstr(5))
      toy_rec = dta_utils.fluctPoisson(rec, rand)
      toy_data = dta_utils.fluctPoisson(toy_rec, rand)
      res = ibu.unfold(toy_data, reco, truth, response, k)
      stats2 = res.GetSumw2()
      for i in range(nBins):
  ##       deno = toy_data.GetBinContent(i)
         deno = truth.GetBinContent(i)
         toy_num = res.GetBinContent(i)
  ##       toy_num = sum(res.GetBinContent(j)*bias_res.GetBinContent(i,j) for j in range(nBins))
         if deno != 0 and stats2[i] != 0:
            toy_res_bias[i] = ((toy_num - deno) / deno)
         else:
            toy_res_bias[i] = 0
      toy.Delete()
      #if n == 2:
      #   print(toy_res_bias)
    ##  bias_sig[n,:] = [ toy_res_bias[i]/stat[i] if stat[i] else 0 for i in range(len(stat))]
      bias_sig[n,:] = [ toy_res_bias[i] for i in range(nBins)]
      total_val[n,:] = [ res.GetBinContent(i) for i in range(nBins) ]
   average_val = total_val.mean(axis=0)
   total_stats = total_val - average_val
   stats_val = np.sqrt(((total_stats ** 2).mean(axis=0)))
   res = np.nan_to_num(np.sqrt(((bias_sig)) ** 2).mean(axis=0)) #(bias_sig/stats_val) ** 2, stats_val/average_val
   for i in range(nBins):
      new.SetBinContent(i, res[i])
      if stats_val[i]:
         stats.SetBinContent(i, stats_val[i]/average_val[i])
      else:
         stats.SetBinContent(i, 0)
   return new, stats


def Get_stats_average(unfolded_list):
   """find the statistical flucation range (uncertainty) for each iteration"""
   N = unfolded_list[0].GetNbinsX() + 2
   new = unfolded_list[0].Clone(dta_utils.randstr(5))
   Ntoys = len(unfolded_list)
   input_val = np.zeros((Ntoys, N))
   stat_average = np.zeros(N)
   stat_sig = np.zeros((Ntoys, N))
   for n in range(Ntoys):
      input_val[n,:] = [ unfolded_list[n].GetBinContent(i) for i in range(N) ]
   stat_average = input_val.mean(axis=0)
   input_val_mean = input_val.mean(axis=0)
   stat_val = input_val - stat_average
   stats = np.sqrt((stat_val ** 2).mean(axis=0))

   # N = plot_list[0].GetNbinsX() + 2
   # new = plot_list[0].Clone(dta_utils.randstr(5))
   # stats = 0
   # for plot in plot_list:
   #    stats += np.sqrt(plot.GetSumw2())
   # stats = stats/len(plot_list)
   for i in range(N):
       if input_val_mean[i]:
          new.SetBinContent(i, stats[i]/input_val_mean[i])
       else:
          new.SetBinContent(i, 0)
   return new


def bias(reco, truth, response, fid, rand, nToys, k):
     nBins = reco.GetNbinsX() + 2
     toy_bias = np.zeros(nToys)
     bias_sig = np.zeros((nToys, nBins))

     fid = rbo.Clone()
     for i in range(nBins):
         if reco.GetBinContent(i) == 0:
            fid.SetBinContent(i, 0)
         else:
            val = rbo.GetBinContent(i)/reco.GetBinContent(i)
            fid.SetBinContent(i, val)
     for n in range(nToys):
         toy_data = dta_utils.fluctPoisson(reco, rand)   #use fluctuated reco as toy_data
         for s in range(len(fid)):
             toy_data.SetBinContent(s, toy_data.GetBinContent(s)*fid[s])
         res = IBUnfold(toy_data, reco, truth, response, k)
      #   deno = fid.apply_fid_corr_to(toy_data)
         for i in range(nBins):
            toy_num = sum(res.GetBinContent(i)*response.GetBinContent(i,j) for j in range(nBins))
            deno = toy_data
            if deno != 0:
               toy_bias[i] = (toy_num - deno) / deno
            else:
               toy_bias[i] = 0
      #   bias_sig[n,:] = [ toy_res_bias[i]/stat[i] for i in range(len(stat)) ]
         bias_sig[n,:] = [ toy_bias[i] for i in range(nBins) ]
     return ((bias_sig ** 2).mean(axis=0))




#def Get_rms_bias(bias_list):
#   N = bias_list[0].GetNbinsX() + 2
#   new = bias_list[0].Clone(dta_utils.randstr(5))
#   bias_sig = np.zeros(N)
#   for plot in bias_list:
#      stats = plot.GetSumw2()
#      for b in range(N):
#         if stats[b]:
#            bias_sig[b] += (plot.GetBinContent(b)/np.sqrt(stats[b]))**2
#         else:
#            bias_sig[b] += 0
#   rms_bias_sig = np.sqrt(bias_sig/len(bias_list))
#   for s in range(N):
#      new.SetBinContent(s, rms_bias_sig[s])
#   return new


def Get_rms_bias(bias_list, unfolded_list, stats):
   """find the rms bias value for the modeling variation bias tests"""
   N = bias_list[0].GetNbinsX() + 2
   new = bias_list[0].Clone(dta_utils.randstr(5))
   Ntoys = len(bias_list)
#   stat_val = np.zeros((Ntoys, N))
#   stat_average = np.zeros(N)
#   stat_sig = np.zeros((Ntoys, N))
#   for n in range(Ntoys):
#      stat_val[n,:] = [ unfolded_list[n].GetBinContent(i) for i in range(N) ]
#   stat_average = stat_val.mean(axis=0)
#   stat_val = (stat_val - stat_average)
#   for n in range(Ntoys):
#      for b in range(N):
#         if not stat_average[b]:
#            stat_val[n][b] = stat_val[n][b]
#         else:
#            stat_val[n][b] = stat_val[n][b]/stat_average[b]
#   stats = np.sqrt((stat_val ** 2).mean(axis=0))
##already is ratio stats
## uncomment if want ratio stats
##   for i in range(N):
##      if not stat_average[i]:
##         continue
##      else:
##         stats[i] = stats[i]/stat_average[i]

   bias_sig = np.zeros(N)
   rms_bias_sig = np.zeros(N)
   for plot in bias_list:
      for b in range(N):
         if stats[b]:
            bias_sig[b] += (plot.GetBinContent(b)/stats.GetBinContent(b))**2  ## stats[b]
         else:
            bias_sig[b] += 0
   for i in range(N):
      if bias_sig[i]:
         rms_bias_sig[i] = np.sqrt(bias_sig[i]/len(bias_list))
      else:
         rms_bias_sig[i] = 0
   for s in range(N):
      new.SetBinContent(s, rms_bias_sig[s])
   return new



def Get_bias(unfolded, truth):
   """calculate the ratio bias (unfolded - truth) / truth"""
   no_bins = unfolded.GetNbinsX() + 2
   res = unfolded.Clone(dta_utils.randstr(5))
   tru = truth.Clone(dta_utils.randstr(5))
   res.Add(tru, -1)
   res.Divide(tru)
   tru.Delete()
   return res


def Get_Bias_Rank(var_list):
   bias_sq = []
   for h in var_list:
      bias_sum = 0
      for i in range(h.GetNbinsX() + 2):
          sq_bias = h.GetBinContent(i)**2
          bias_sum += sq_bias
      bias_sq.append(bias_sum)
   return bias_sq

